<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'firstname' => 'admin',
            'lastname' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'user_group_id' => App\User_group::where('name' , 'Administrator')->first()->id
        ]);

        User::create([
            'firstname' => 'recruitment',
            'lastname' => 'recruitment',
            'email' => 'recruitment@recruitment.com',
            'password' => bcrypt('recruitment'),
            'user_group_id' => App\User_group::where('name' , 'Recruitment')->first()->id
        ]);
    }
}
