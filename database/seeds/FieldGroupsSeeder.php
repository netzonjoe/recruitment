<?php

use Illuminate\Database\Seeder;
use App\Field_Group;
class FieldGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Field_Group::create([
            'name' => 'Educational Background',
            'icon' => 'fa fa-graduation-cap'
        ]);

        Field_Group::create([
            'name' => 'Employment Details',
            'icon' => 'fa fa-building'
        ]); 
        
        Field_Group::create([
            'name' => 'Identification Numbers',
            'icon' => 'fa fa-child'
        ]); 
    }
}
