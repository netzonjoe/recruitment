<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserGroupsSeeder::class);
        $this->call(UserSuperAdminSeeder::class);
        $this->call(FieldGroupsSeeder::class);
        $this->call(CustomFieldsSeeder::class);
        
    }
}
