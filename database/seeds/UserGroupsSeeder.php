<?php

use Illuminate\Database\Seeder;
use App\User_group;

class UserGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User_group::create([
            'name' => 'Administrator',
            'description' => 'Administrator user'
        ]);

        User_group::create([
            'name' => 'Recruitment',
            'description' => 'Recruitment team'
        ]);  
    }
}

