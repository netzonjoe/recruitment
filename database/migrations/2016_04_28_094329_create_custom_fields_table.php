<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomFieldsTable extends Migration {

	public function up()
	{
		Schema::create('custom_fields', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('model_name');
			$table->string('label');
			$table->string('placeholder');
			$table->boolean('is_required')->default(true);
			$table->string('type_field');
			$table->binary('choices');
			$table->boolean('status');
			$table->integer('field_group_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('custom_fields');
	}
}