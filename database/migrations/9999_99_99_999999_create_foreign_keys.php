<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('users', function(Blueprint $table) {
			$table->foreign('user_group_id')->references('id')->on('user_groups')
						->onDelete('restrict')
						->onUpdate('no action');
		});
		Schema::table('custom_fields', function(Blueprint $table) {
			$table->foreign('field_group_id')->references('id')->on('field_groups')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('additional_infos', function(Blueprint $table) {
			$table->foreign('applicant_id')->references('id')->on('applicants');
			$table->foreign('custom_field_id')->references('id')->on('custom_fields');
		});

		Schema::table('questionnaire_pages', function(Blueprint $table) {
			$table->foreign('questionnaire_id')->references('id')->on('questionnaires')
				->onDelete('cascade');
		});

		Schema::table('applicant_answers', function(Blueprint $table) {
			$table->foreign('questionnaire_id')->references('id')->on('questionnaires');
			$table->foreign('applicant_id')->references('id')->on('applicants');
		});

		Schema::table('notifications', function(Blueprint $table) {
			$table->foreign('questionnaire_id')->references('id')->on('questionnaires');
			$table->foreign('applicants_id')->references('id')->on('applicants');
		});
	}

	public function down()
	{
		Schema::table('users', function(Blueprint $table) {
			$table->dropForeign('users_user_group_id_foreign');
		});
		Schema::table('additional_infos', function(Blueprint $table) {
			$table->dropForeign('additional_infos_custom_field_id_foreign');
			$table->dropForeign('additional_infos_applicant_id_foreign');
		});
		Schema::table('custom_fields', function(Blueprint $table) {
			$table->dropForeign('custom_fields_field_group_id_foreign');
		});

		Schema::table('questionnaire_pages', function(Blueprint $table) {
			$table->dropForeign('questionnaire_pages_questionnaire_id_foreign');
		});
		Schema::table('applicant_answers', function(Blueprint $table) {
			$table->dropForeign('applicant_answers_applicant_id_foreign');
			$table->dropForeign('applicant_answers_questionnaire_id_foreign');
		});
		Schema::table('notifications', function(Blueprint $table) {
			$table->dropForeign('notifications_applicants_id_foreign');
			$table->dropForeign('notifications_questionnaire_id_foreign');
		});
	}
}