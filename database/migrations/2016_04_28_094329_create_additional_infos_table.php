<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdditionalInfosTable extends Migration {

	public function up()
	{
		Schema::create('additional_infos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('value')->nullable();
			$table->integer('applicant_id')->unsigned();
			$table->integer('custom_field_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('additional_infos');
	}
}