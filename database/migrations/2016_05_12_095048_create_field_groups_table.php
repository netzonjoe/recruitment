<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFieldGroupsTable extends Migration {

	public function up()
	{
		Schema::create('field_groups', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->string('icon');
		});
	}

	public function down()
	{
		Schema::drop('field_groups');
	}
}