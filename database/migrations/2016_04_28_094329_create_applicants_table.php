<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantsTable extends Migration {

	public function up()
	{
		Schema::create('applicants', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('firstname');
			$table->string('lastname');
			$table->string('middlename');
			$table->string('gender', 6);
			$table->string('email');
			$table->string('contact_number');
			$table->timestamp('date_of_birth');
			$table->string('birth_place');
			$table->string('civil_status', 10);
			$table->string('present_address');
			$table->string('permanent_address');
			$table->string('position_applied');
		});
	}

	public function down()
	{
		Schema::drop('applicants');
	}
}