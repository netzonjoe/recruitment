<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant_answer extends Model {

	protected $table = 'applicant_answers';

	protected $fillable = [
		'applicant_id',
		'questionnaire_id',
		'result',
		'total_number_questions',
		'score'
	];
	protected $dates = ['created_at', 'updated_at'];

	public function applicants()
	{
		return $this->hasMany('App\Applicant', 'id');
	}
}