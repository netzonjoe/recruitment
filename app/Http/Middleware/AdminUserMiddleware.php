<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class AdminUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $user_type = Auth::user()->userGroup()->get()->toArray()[0]['name'];
            switch (strtolower($user_type)) {
                case 'administrator':
                    return $next($request);
                    break;
                
                case 'candidate':
                    return redirect(action('CandidateDashboardController@index'));
                    break;

                case 'employer':
                    return redirect(action('EmployerDashboardController@index'));
                    break;
            }
        }
        return redirect('/');
    }
}

