<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});




// Route::get('/home', 'HomeController@index');




Route::group(['middleware' => ['web']], function () {

    Route::auth();
    Route::get('/guest', 'GuestController@index');
    Route::post('/guest/save-personal-info', 'GuestController@savePersonalInfo');
    Route::post('/guest/save-custom-field-info', 'GuestController@saveCustomFieldInfo');
    Route::post('/guest/get-user-groups', 'GuestController@getAllFieldGroupsAjax');
    Route::post('/guest/get-positions-applied', 'GuestController@getPositionsAppliedAjax');
    Route::post('/guest/save-employee-information', 'GuestController@saveEmployeeInformation');

    Route::post('/guest/get-all-questionaires', 'GuestController@getAllQuestionnaires');
    Route::post('/guest/submit-all-answers', 'GuestController@submitAllAnswers');


    Route::group(['middleware' => 'admin_user'] , function () {

        Route::get('/admin-dashboard', 'AdminDashboardController@index');
        Route::post('/admin-dashboard/get-all-user-groups', 'AdminDashboardController@getAllUserGroupsAjax');
        Route::post('/admin-dashboard/create-user-group', 'AdminDashboardController@createUserGroup');
        Route::post('/admin-dashboard/delete-user-group', 'AdminDashboardController@deleteUserGroup');
        Route::post('/admin-dashboard/update-user-group', 'AdminDashboardController@updateUserGroup');

        Route::post('/admin-dashboard/get-all-field-groups', 'AdminDashboardController@getAllFieldGroupsAjax');
        Route::post('/admin-dashboard/create-field-group', 'AdminDashboardController@createFieldGroup');
        Route::post('/admin-dashboard/delete-field-group', 'AdminDashboardController@deleteFieldGroup');
        Route::post('/admin-dashboard/update-field-group', 'AdminDashboardController@updateFieldGroup');
        


        Route::post('/admin-dashboard/get-all-users', 'AdminDashboardController@getAllUsersAjax');
        Route::post('/admin-dashboard/create-user', 'AdminDashboardController@createUser');
        Route::post('/admin-dashboard/delete-user', 'AdminDashboardController@deleteUser');
        Route::post('/admin-dashboard/update-user', 'AdminDashboardController@updateUser');

        Route::post('/admin-dashboard/get-all-fields', 'AdminDashboardController@getAllFieldsAjax'); 
        Route::post('/admin-dashboard/create-field', 'AdminDashboardController@createField');
        Route::post('/admin-dashboard/delete-field', 'AdminDashboardController@deleteField');
        Route::post('/admin-dashboard/update-field', 'AdminDashboardController@updateField');

        Route::post('/admin-dashboard/get-all-questionaire-groups', 'AdminDashboardController@getAllQuestionaireGroupsAjax'); 
        Route::post('/admin-dashboard/create-questionaire-group', 'AdminDashboardController@createQuestionaireGroup');
        Route::post('/admin-dashboard/delete-questionaire-group', 'AdminDashboardController@deleteQuestionaireGroup');
        Route::post('/admin-dashboard/update-questionaire-group', 'AdminDashboardController@updateQuestionaireGroup');

        Route::post('/admin-dashboard/get-all-question-levels', 'AdminDashboardController@getAllQuestionLevelsAjax'); 
        Route::post('/admin-dashboard/create-question-level', 'AdminDashboardController@createQuestionLevel');
        Route::post('/admin-dashboard/delete-question-level', 'AdminDashboardController@deleteQuestionLevel');
        Route::post('/admin-dashboard/update-question-level', 'AdminDashboardController@updateQuestionLevel');

        Route::post('/admin-dashboard/get-all-articles', 'AdminDashboardController@getAllArticlesAjax'); 
        Route::post('/admin-dashboard/create-article', 'AdminDashboardController@createArticle');
        Route::post('/admin-dashboard/delete-article', 'AdminDashboardController@deleteArticle');
        Route::post('/admin-dashboard/update-article', 'AdminDashboardController@updateArticle');

        Route::post('/admin-dashboard/get-all-questions', 'AdminDashboardController@getAllQuestionsAjax'); 
        Route::post('/admin-dashboard/create-question', 'AdminDashboardController@createQuestion');
        Route::post('/admin-dashboard/delete-question', 'AdminDashboardController@deleteQuestion');
        Route::post('/admin-dashboard/update-question', 'AdminDashboardController@updateQuestion');

        Route::post('/admin-dashboard/get-questionnaires', 'AdminDashboardController@getAllQuestionnaires');
        Route::post('/admin-dashboard/get-questionnaire', 'AdminDashboardController@getQuestionnaireById');
        Route::post('/admin-dashboard/create-questionnaires', 'AdminDashboardController@createNewQuestionnaire');
        Route::post('/admin-dashboard/create-new-page', 'AdminDashboardController@createNewPage');
        Route::post('/admin-dashboard/save-page-changes', 'AdminDashboardController@savePageChanges');
        Route::post('/admin-dashboard/delete-page', 'AdminDashboardController@deletePage');
        Route::post('/admin-dashboard/get-all-applicant-submission', 'AdminDashboardController@getAllApplicantSubmission');
        Route::post('/admin-dashboard/get-all-questionnaires-by-applicant-id', 'AdminDashboardController@getAllQuestionnairesByApplicantId');
        Route::post('/admin-dashboard/get-all-notifications', 'AdminDashboardController@getAllNotifications');
        Route::post('/admin-dashboard/view-notification', 'AdminDashboardController@viewNotification');
        Route::post('/admin-dashboard/get-grade-results-by-applicant-id', 'AdminDashboardController@getGradeResultsByApplicantId');

        // Route::post('/admin-dashboard/update-member', 'AdminDashboardController@updateMember');
        // Route::post('/admin-dashboard/delete-member', 'AdminDashboardController@deleteMember');
        // Route::post('/admin-dashboard/delete-member', 'AdminDashboardController@deleteMember');
        // Route::post('/admin-dashboard/update-member-status', 'AdminDashboardController@updateMemberStatus');

        // Route::post('/admin-dashboard/create-job', 'AdminDashboardController@createJob');
        // Route::post('/admin-dashboard/delete-job', 'AdminDashboardController@deleteJob');
        // Route::post('/admin-dashboard/update-job', 'AdminDashboardController@updateJob');
        // Route::post('/admin-dashboard/clone-job', 'AdminDashboardController@cloneJob');
        // Route::post('/admin-dashboard/get-all-expired-jobs', 'AdminDashboardController@getAllExpiredJobsAjax');
        // Route::post('/admin-dashboard/repost-job', 'AdminDashboardController@repostJob');
        
    });
        
});