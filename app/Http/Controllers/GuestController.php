<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Auth;
use App\User_group;
use App\User;
use App\Applicant;
use App\Field_group;
use App\Custom_field;
use App\Questionnaire;
use App\Applicant_answer;
use App\Questionnaire_page;
use App\Notification;
use Route;
use DB;
use Acme\Rest;
use Acme\GlobalCtrlHelper;
use Carbon\Carbon;
use Config;
use Exception;
use File;
use Response;
use Session;

class GuestController extends Controller
{
	public function index()
	{
		$_token = csrf_token();

        $route_list = json_encode(GlobalCtrlHelper::getRouteList('GuestController'));
        
        $angular_directive_alert = GlobalCtrlHelper::getAngularDirectiveAlert();

   		return view('guest_dashboard.index', compact('_token', 'route_list', 'angular_directive_alert'));
	}

	public function getAllFieldGroupsAjax()
	{
		$field_groups = Field_group::orderBy('created_at', 'asc')->get()->toArray();

		foreach ($field_groups as $group_key => $field_group)
		{
			$field_groups[$group_key]['fields'] = Custom_field::where('field_group_id', $field_group['id'])->get()->toArray();

			foreach ($field_groups[$group_key]['fields'] as $field_key => $value)
			{
				$model_name = $field_groups[$group_key]['fields'][$field_key]['model_name'];

				$field_groups[$group_key]['fields'][$model_name] = $field_groups[$group_key]['fields'][$field_key];
				$field_groups[$group_key]['fields'][$model_name]['choices'] = unserialize($field_groups[$group_key]['fields'][$field_key]['choices']);

				unset($field_groups[$group_key]['fields'][$field_key]);
			}
		}

		return Rest::success(['$field_groups' => $field_groups]);
	}

	public function saveCustomFieldInfo(Request $request)
	{
		if ($request->ajax())
		{
			$params = $request->all();

			$fields = array();
			$request = array();
			$custom_field_data = array();

			foreach ($params as $key => $param)
			{	
				if (isset($param['is_required']) && $param['is_required'])
					$fields[$key] = 'required';

				if ($key !== '_token' && $key !== 'lastStep')
				{
					$request[$key] = isset($param['value']) ? $param['value'] : '';
					$custom_field_data[$key] = ['custom_field_id' => $param['id'], 'value' => isset($param['value']) ? $param['value'] : ''];
				}
			}

			$validator = Validator::make($request, $fields);

			if ($validator->fails())
			{
				return Rest::failed($validator);
			}

			$other_field = Session::has('employment_other_fields_info') ? Session::get('employment_other_fields_info') : array();
			
			foreach ($custom_field_data as $key => $value)
			{
				if (!array_key_exists($key, $other_field))
				{
					$other_field[$key] = $value;
				}
			}

			/* TODO: if ($param['lastStep']) is true then insert the applicant information including other_fields_info to the database  */

			Session::set('employment_other_fields_info', $other_field);

			return Rest::success(['$other_fields_info' => $other_field, 'msg' => 'Custom field was created successfully']);
		}
	}

	public function getPositionsAppliedAjax(Request $request) {
		if ($request->ajax()) {
			$questionnaires = Questionnaire::all()->toArray();
			return Rest::success(['$questionnaires' => $questionnaires]);	
		}
	}

	public function saveEmployeeInformation(Request $request) {
		if ($request->ajax()) {
			return Rest::success(['proceed' => true]);	
			// dd(Session::get('employment_personal_info') + Session::get('employment_other_fields_info'));	
		}
	}

	public function savePersonalInfo(Request $request)
	{
		if ($request->ajax()) {
			$params = $request->all();

			$fields = [
				'firstname' => 'required|min:3|max:255',
				'lastname' => 'required|min:3|max:255',
				'middlename' => 'required|min:3|max:255',
				'contact_number' => 'required',
				'email' => 'required|email|unique:applicants,email',
				'birth_place' => 'required|min:3',
				'gender' => 'required',
				'civil_status' => 'required',
				'permanent_address' => 'required',
				'present_address' => 'required',
				'position_applied' => 'required'
			];

			$validator = Validator::make($params, $fields);

			if ($validator->fails())
			{
				return Rest::failed($validator);
			}

			/*$applicant = new Applicant([
				'firstname' => $params['firstname'],
				'lastname' => $params['lastname'],
				'middlename' => $params['middlename'],
				'gender' => $params['gender'],
				'email' => $params['email'],
				'contact_number' => $params['contact_number'],
				'civil_status' => $params['civil_status'],
				'date_of_birth' => time(),
				'birth_place' => $params['birth_place']
			]);

			$applicant->save();*/
			$personal_info = [
				'firstname' => $params['firstname'],
				'lastname' => $params['lastname'],
				'middlename' => $params['middlename'],
				'gender' => $params['gender'],
				'email' => $params['email'],
				'contact_number' => $params['contact_number'],
				'civil_status' => $params['civil_status'],
				'date_of_birth' => time(),
				'birth_place' => $params['birth_place'],
				'present_address' => $params['present_address'],
				'permanent_address' => $params['permanent_address'],
				'position_applied' => $params['position_applied']
			];

			Session::set('employment_personal_info', $personal_info);

			return Rest::success(['$personal_info' => $personal_info, 'msg' => 'Personal Information was created successfully']);
		}
	}

	public function getAllQuestionnaires() {
		if (Session::has('employment_personal_info')) {
			$questionnaire_page = Questionnaire_page::find(Session::get('employment_personal_info')['position_applied'])->get()->toArray();
			foreach ($questionnaire_page as $key => $value) {
				$questionnaire_page[$key]['page_items'] = unserialize($value['page_items']);
			}
			return Rest::success(['$questionnaire_page' => $questionnaire_page]);
		} else {
			return Rest::success(['$questionnaire_page' => false]);
		}
	}

	public function submitAllAnswers (Request $request) {
		if ($request->ajax()) {
			$params = $request->all();
			if (count($params['data']) > 0) {
				$questionnaire_id = $params['data'][0]['questionnaire_id'];
				$result = serialize($params['data']);
				$score = 0;
				$total_number_questions = 0;
				foreach ($params['data'] as $key => $value) {
					foreach ($value['page_items'] as $key => $val) {
						if ($val['field_type'] === 'question') {
							$answers = $val['attributes']['answers'];
							$applicant_answer = $val['attributes']['applicant_answer'];
							$total_number_questions += 1;
							if (is_array($answers) && is_array($applicant_answer)) {
								if (count($answers) >= count($applicant_answer)) {
									$intersects = array_intersect($answers, $applicant_answer);
									if (count($intersects) === count($answers)) {
										$score += 1;
									}
								}
							} else {
								if (strtolower($answers) === strtolower($applicant_answer)) {
									$score += 1;
								}
							}
						}
					}
				}
				
			}
			$employment_personal_info = Session::get('employment_personal_info');

			$applicant = new Applicant([
					'firstname' => $employment_personal_info['firstname'],
					'lastname' => $employment_personal_info['lastname'],
					'middlename' => $employment_personal_info['middlename'],
					'gender' => $employment_personal_info['gender'],
					'email' => $employment_personal_info['email'],
					'contact_number' => $employment_personal_info['contact_number'],
					'civil_status' => $employment_personal_info['civil_status'],
					'date_of_birth' => time(),
					'birth_place' => $employment_personal_info['birth_place'],
					'present_address' => $employment_personal_info['present_address'],
					'permanent_address' => $employment_personal_info['permanent_address'],
					'position_applied' => $employment_personal_info['position_applied']
			]);

			$applicant->save();

			$applicant_answer = new Applicant_answer ([
				'applicant_id' => $applicant->id,
				'questionnaire_id' => $questionnaire_id, 
				'result' => $result, 
				'score' => $score, 
				'total_number_questions' => $total_number_questions
			]);

			$applicant_answer->save();

			/* Add notification */
			$notification = new Notification();
			$notification->applicants_id = $applicant->id;
			$notification->questionnaire_id = $questionnaire_id;

			$notification->save();

			$request->session()->forget('employment_personal_info');
			$request->session()->forget('employment_other_fields_info');

			$request->session()->flush();

			$options = array(
				'encrypted' => true
			);
			
			$pusher = new \Pusher(
				'41b2d387872081e26127',
				'4a69b0c12738277319f3',
				'216531',
				$options
			);

			$data['message'] = 'Refresh data on applicant submission';
			$pusher->trigger('applicant_submition_channel', 'refresh_applicant_submition_event', $data);


			return Rest::success(['redirect' => true]);
		}
	}
}