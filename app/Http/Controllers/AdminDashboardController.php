<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Auth;
use App\Custom_field;
use App\User_group;
use App\Questionaire_group;
use App\Questionnaire;
use App\Questionnaire_page;
use App\Applicant_answer;
use App\Question_level;
use App\Article;
use App\Applicant;
use App\Question;
use App\Field_group;
use App\Notification;
use App\User;
use Route;
use DB;
use Acme\Rest;
use Acme\GlobalCtrlHelper;
use Carbon\Carbon;
use Config;
use Exception;
use File;
use Log;


class AdminDashboardController extends Controller
{

	public function index()
	{
		$_token = csrf_token();

        $route_list = json_encode(GlobalCtrlHelper::getRouteList('AdminDashboardController'));
        
        $angular_directive_alert = GlobalCtrlHelper::getAngularDirectiveAlert();

   		return view('admin_dashboard.index', compact('_token', 'route_list', 'angular_directive_alert'));
	}

	/*----User-Group-------------------------------------------------------------------*/
	public function getAllUserGroupsAjax()
	{
		return Rest::success(['$user_groups' => $this->getAllUserGroups()]);
	}

	protected function getAllUserGroups()
	{
		return User_group::whereNotIn('name', ['Administrator'])->orderBy('created_at', 'desc')->get()->toArray();
	}

	protected function createUserGroupValidator(array $data, $option = []) {
        $fields = [
            'name' => 'required|unique:user_groups',
            'description' => 'required|min:3|max:1000'
        ];
        return Validator::make($data + $option, $fields);
    }

	public function createUserGroup(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $validator = $this->createUserGroupValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

	        $user_group = new User_group([
	            'name' => $params['name'],
	            'description' => $params['description']
	        ]);

	        $user_group->save();

	        $user_groups = $this->getAllUserGroups();

            return Rest::success(['$user_groups' => $user_groups, 'msg' => 'User Group information was created successfully']);
        }
	}
	public function deleteUserGroup(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

	        User_group::destroy($request->get('id'));

	        $user_groups = $this->getAllUserGroups();

            return Rest::success(['$user_groups' => $user_groups, 'msg' => 'User Group information was deleted successfully']);
        }
	}

	protected function updateUserGroupValidator(array $data, $option = []) {
        $fields = [
            'name' => 'required',
            'description' => 'required|min:3|max:1000'
        ];
        return Validator::make($data + $option, $fields);
    }

	public function updateUserGroup(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $validator = $this->updateUserGroupValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $allowUpdate = false;        
	        $ifExist = User_group::where('name' , $params['name'])->first();
	        if ($ifExist !== null) {
	            if ($params['id'] == $ifExist->id) {
	                $allowUpdate = true;
	            }
	        } else {
	            $allowUpdate = true;
	        }
	        
	        if ($allowUpdate) {
	            $user_group = User_group::find($params['id']);
	            $user_group->name = $params['name'];
	            $user_group->description = $params['description'];
	            $user_group->save();
	        } else {
	        	return Rest::dataExist(['name' => ['User Group Name already exist.']]);
	        }

	        $user_groups = $this->getAllUserGroups();

            return Rest::success(['$user_groups' => $user_groups, 'msg' => 'User Group information was updated successfully']);
        }
	}
	/*---#User-Group-------------------------------------------------------------------*/

	/*----User-------------------------------------------------------------------------*/
	public function getAllUsersAjax()
	{	
		return Rest::success(['$users' => $this->getAllUsers(), '$users_counter' => $this->getNumberOfActiveAndInactiveUsers()]);
	}

	protected function getAllUsers()
	{
		$users = DB::table('user_groups')
            ->whereNotIn('user_groups.name', ['administrator'])
            ->join('users', 'users.user_group_id', '=', 'user_groups.id')
            ->select('users.id', 'users.user_group_id', 'users.firstname', 'users.lastname', 'users.email', 'users.status', DB::raw('DATE_FORMAT(users.created_at, \'%M %d, %Y\') as created_at'), 
                'user_groups.name','user_groups.description' )
            ->orderBy('users.created_at', 'desc')
            ->get();
        return json_decode(json_encode($users), true);
	}

	protected function getNumberOfActiveAndInactiveUsers()
	{
		$active_users = DB::table('users')
            ->whereNotIn('user_group_id' , [User_group::where('name' , 'Administrator')->first()->id])
            ->where('status', true)
            ->select(DB::raw('count(users.id) as active_user'))
            ->get();
        $inactive_users = DB::table('users')
            ->whereNotIn('user_group_id' , [User_group::where('name' , 'Administrator')->first()->id])
            ->where('status', false)
            ->select(DB::raw('count(users.id) as inactive_users'))
            ->get();
        return ['active_users' => $active_users[0]->active_user,
        		'inactive_users' => $inactive_users[0]->inactive_users];
	}

	protected function createUserValidator(array $data, $option = []) {
        $fields = [
        	'user_group_id' => 'required',
            'firstname' => 'required|min:3|max:255',
            'lastname' => 'required|min:3|max:255',
            'email' => 'required|email|min:3|max:255|unique:users',
            'password' => 'required|min:6',
            'status' => 'required'
        ];

        return Validator::make($data + $option, $fields);
    }

	public function createUser(Request $request){
		if ($request->ajax()) { 
            $params = $request->all();

            $validator = $this->createUserValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

	        $user_group = User_group::find($params['user_group_id']);
	        $user_group->userDetails()->save(new User([
	            'firstname' => $params['firstname'],
	            'lastname' => $params['lastname'],
	            'email' => $params['email'],
	            'status' => ($params['status'] === 'Active') ? true: false,
	            'password' => bcrypt($params['password'])
	        ]));

	        $user_group->save();

	        $users = $this->getAllUsers();
	        $users_counter = $this->getNumberOfActiveAndInactiveUsers();
            return Rest::success(['$users' => $users, '$users_counter' => $users_counter, 'msg' => 'User information was created successfully']);
        }
	}

	public function deleteUser(Request $request) {
		if ($request->ajax()) { 
	        User::destroy($request->get('id'));
	        $users = $this->getAllUsers();
	        $users_counter = $this->getNumberOfActiveAndInactiveUsers();
            return Rest::success(['$users' => $users,'$users_counter' => $users_counter, 'msg' => 'User information was deleted successfully']);
        }
	}

	protected function updateUserValidator(array $data, $option = []) {
        $fields = [
            'user_group_id' => 'required',
            'firstname' => 'required|min:3|max:255',
            'lastname' => 'required|min:3|max:255',
            'email' => 'required|email|min:3|max:255',
            'password' => 'min:6',
            'status' => 'required'
        ];
        return Validator::make($data + $option, $fields);
    }

	public function updateUser(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $validator = $this->updateUserValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $allowUpdate = false;        
	        $ifExist = User::where('email' , $params['email'])->first();
	        if ($ifExist !== null) {
	            if ($params['id'] == $ifExist->id) {
	                $allowUpdate = true;
	            }
	        } else {
	            $allowUpdate = true;
	        }
	        	
	        if ($allowUpdate) {
	            $user = User::find($params['id']);
	            $user->user_group_id = $params['user_group_id'];
	            $user->firstname = $params['firstname'];
	            $user->lastname = $params['lastname'];
	            $user->email = $params['email'];
	            if (isset($params['password'])) {
	            	$user->password = bcrypt($params['password']);
	            }
	            $user->status = ($params['status'] === 'Active') ? true: false;
	            $user->save();
	        } else {
	        	return Rest::dataExist(['email' => ['User email already exist.']]);
	        }

	        $users = $this->getAllUsers();

            return Rest::success(['$users' => $users, 'msg' => 'User information was updated successfully']);
        }
	}
	/*---#User-------------------------------------------------------------------------*/

	/*----Fields-------------------------------------------------------------------------*/
	public function getAllFieldsAjax()
	{	
		return Rest::success(['$fields' => $this->getAllFields()]);
	}

	protected function getAllFields()
	{
		$fields = DB::table('custom_fields')
            // ->whereNotIn('user_groups.name', ['administrator'])
            ->join('field_groups', 'custom_fields.field_group_id', '=', 'field_groups.id')
            ->select('custom_fields.*', 'field_groups.name', 'field_groups.icon', DB::raw('DATE_FORMAT(custom_fields.created_at, \'%M %d, %Y\') as created_at'))
            ->orderBy('custom_fields.created_at', 'desc')
            ->get();

        $fields = json_decode(json_encode($fields), true);
        foreach ($fields as $key => $value) {
        	$value['choices'] = (isset($value['choices'])) ? unserialize($value['choices']) : '[]';
        	$fields[$key] = $value;
        }
        return $fields;
	}

	protected function fieldValidator(array $data, $option = []) {
        $fields = [
        	'field_group_id' => 'required',
        	'model_name' => 'required|min:3|max:255|unique:custom_fields',
            'label' => 'required|min:3|max:255',
            'placeholder' => 'min:3|max:255',
            'type_field' => 'required',
            'status' => 'required'
        ];

        return Validator::make($data , $fields + $option);
    }

	public function createField(Request $request){
		if ($request->ajax()) { 
            $params = $request->all();

			$hasChoices = array('checkbox', 'radio-button', 'drop-down');
			$additional = [];
			
			if (in_array($params['type_field'], $hasChoices)) {
				$additional = ['choices' => 'required'];

				if (empty($params['choices'][0][0]['value']) && empty($params['choices'][0][1]['value']))
					unset($params['choices']);
			}

            $validator = $this->fieldValidator($params, $additional);

            if($validator->fails()) {
                return Rest::failed($validator);
            }

             // 'field_group_id', 'label', 'placeholder', 'type_field', 'is_required', 'choices', 'status'
	        (new Custom_field([
	        	'field_group_id' => $params['field_group_id'],
	        	'model_name' => $params['model_name'],
	            'label' => $params['label'],
	            'placeholder' => (isset($params['placeholder'])) ? $params['placeholder'] : '',
	            'type_field' => $params['type_field'],
	            'is_required' => $params['is_required'],
	            'choices' => (isset($params['choices'])) ? serialize($params['choices']) : serialize('[{key : \'value\',value: \'\'},{key : \'label\', value: \'\'}]'),
	            'status' => ($params['status'] === 'Enable') ? true : false
	        ]))->save();

	        $fields = $this->getAllFields();
	        return Rest::success(['$fields' => $fields, 'msg' => 'Custom Field information was created successfully']);
        }
	}

	public function deleteField(Request $request) {
		if ($request->ajax()) { 
	        Custom_field::destroy($request->get('id'));
	        $fields = $this->getAllFields();
            return Rest::success(['$fields' => $fields, 'msg' => 'Custom Field information was deleted successfully']);
        }
	}

	public function updateField(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $hasChoices = array('checkbox', 'radio-button', 'drop-down');
			$additional = [];
			if (isset($params['type_field'])) {
				if (in_array($params['type_field'], $hasChoices)) {
					$additional = ['choices' => 'required'];
				}
			}

            $validator = $this->fieldValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $field = Custom_field::find($params['id']);
            $field->field_group_id = $params['field_group_id'];
            $field->model_name = $params['model_name'];
            $field->label = $params['label'];
            $field->placeholder = (isset($params['placeholder'])) ? $params['placeholder'] : '';
            $field->type_field = $params['type_field'];
            $field->is_required = $params['is_required'];
            $field->choices = (isset($params['choices'])) ? serialize($params['choices']) : '[]';
            $field->status = ($params['status'] === 'Enable') ? true: false;
            $field->save();

	        $fields = $this->getAllFields();

            return Rest::success(['$fields' => $fields, 'msg' => 'Custom Field information was updated successfully']);
        }
	}
	/*---#Fields------------------------------------------------------------------------*/



	/*----Field-Group-------------------------------------------------------------------*/
	public function getAllFieldGroupsAjax()
	{
		return Rest::success(['$field_groups' => $this->getAllFieldGroups()]);
	}

	protected function getAllFieldGroups()
	{
		return Field_group::orderBy('created_at', 'desc')->get()->toArray();
	}

	protected function createFieldGroupValidator(array $data, $option = []) {
        $fields = [
            'icon' => 'required'
        ];
        return Validator::make($data , $fields + $option);
    }

	public function createFieldGroup(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $additional = ['name' => 'required|unique:field_groups'];
            $validator = $this->createFieldGroupValidator($params, $additional);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

	        $field_group = new Field_group([
	            'name' => $params['name'],
	            'icon' => $params['icon']
	        ]);

	        $field_group->save();

	        $field_groups = $this->getAllFieldGroups();

            return Rest::success(['$field_groups' => $field_groups, 'msg' => 'Field Group information was created successfully']);
        }
	}
	public function deleteFieldGroup(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

	        Field_group::destroy($request->get('id'));

	        $field_groups = $this->getAllFieldGroups();

            return Rest::success(['$field_groups' => $field_groups, 'msg' => 'Field Group information was deleted successfully']);
        }
	}

	public function updateFieldGroup(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $additional = ['name' => 'required'];
            $validator = $this->createFieldGroupValidator($params, $additional);
            if($validator->fails()) {
                return Rest::failed($validator);
            }


            $allowUpdate = false;        
	        $ifExist = Field_group::where('name' , $params['name'])->first();
	        if ($ifExist !== null) {
	            if ($params['id'] == $ifExist->id) {
	                $allowUpdate = true;
	            }
	        } else {
	            $allowUpdate = true;
	        }
	        	
	        if ($allowUpdate) {
	            $field_group = Field_group::find($params['id']);
	            $field_group->name = $params['name'];
	            $field_group->icon = $params['icon'];
	            $field_group->save();
	        } else {
	        	return Rest::dataExist(['name' => ['The name has already been taken.']]);
	        }

	        $field_groups = $this->getAllFieldGroups();

            return Rest::success(['$field_groups' => $field_groups, 'msg' => 'Field Group information was updated successfully']);
        }
	}
	/*---#Field-Group-------------------------------------------------------------------*/

	/*----Questionaire-Group-------------------------------------------------------------------*/
	public function getAllQuestionaireGroupsAjax()
	{
		return Rest::success(['$questionaire_groups' => $this->getAllQuestionaireGroups()]);
	}

	protected function getAllQuestionaireGroups()
	{
		return Questionaire_group::orderBy('created_at', 'desc')->get()->toArray();
	}

	protected function questionaireGroupValidator(array $data, $option = []) {
        $fields = [
            'description' => 'min:3'
        ];
        return Validator::make($data , $fields + $option);
    }

	public function createQuestionaireGroup(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $additional = ['name' => 'required|unique:questionaire_groups'];
            $validator = $this->questionaireGroupValidator($params, $additional);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

	        $questionaire_group = new Questionaire_group([
	            'name' => $params['name'],
	            'description' => $params['description']
	        ]);

	        $questionaire_group->save();

	        $questionaire_groups = $this->getAllQuestionaireGroups();

            return Rest::success(['$questionaire_groups' => $questionaire_groups, 'msg' => 'Questionaire Group information was created successfully']);
        }
	}
	public function deleteQuestionaireGroup(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

	        Questionaire_group::destroy($request->get('id'));

	        $questionaire_groups = $this->getAllQuestionaireGroups();

            return Rest::success(['$questionaire_groups' => $questionaire_groups, 'msg' => 'Questionaire Group information was deleted successfully']);
        }
	}

	public function updateQuestionaireGroup(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $additional = ['name' => 'required'];
            $validator = $this->questionaireGroupValidator($params, $additional);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $allowUpdate = false;        
	        $ifExist = Questionaire_group::where('name' , $params['name'])->first();
	        if ($ifExist !== null) {
	            if ($params['id'] == $ifExist->id) {
	                $allowUpdate = true;
	            }
	        } else {
	            $allowUpdate = true;
	        }
	        	
	        if ($allowUpdate) {
	            $questionaire_group = Questionaire_group::find($params['id']);
	            $questionaire_group->name = $params['name'];
	            $questionaire_group->description = $params['description'];
	            $questionaire_group->save();
	        } else {
	        	return Rest::dataExist(['name' => ['The name has already been taken.']]);
	        }

	        $questionaire_groups = $this->getAllQuestionaireGroups();

            return Rest::success(['$questionaire_groups' => $questionaire_groups, 'msg' => 'Questionaire Group information was updated successfully']);
        }
	}
	/*---#Questionaire-Group-------------------------------------------------------------------*/

	/*----Question-Level-------------------------------------------------------------------*/
	public function getAllQuestionLevelsAjax(Request $request)
	{
		$params = $request->all();
		$questionaire_group_id = isset($params['questionaire_group_id']) ? $params['questionaire_group_id'] : '';
		return Rest::success(['$question_levels' => $this->getAllQuestionLevels($questionaire_group_id)]);
	}

	protected function getAllQuestionLevels($questionaire_group_id = '')
	{	
		if ($questionaire_group_id === '') {
			return Question_level::orderBy('created_at', 'desc')->get()->toArray();
		} else {
			return Question_level::where('questionaire_group_id', $questionaire_group_id)->orderBy('created_at', 'desc')->get()->toArray();
		}
	}

	protected function questionLevelValidator(array $data, $option = []) {
        $fields = [
        	'questionaire_group_id' => 'required',
            'description' => 'min:3'
        ];
        return Validator::make($data , $fields + $option);
    }

	public function createQuestionLevel(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $additional = ['name' => 'required|unique:question_levels'];
            $validator = $this->questionLevelValidator($params, $additional);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

	        $question_level = new Question_level([
	        	'questionaire_group_id' => $params['questionaire_group_id'],
	            'name' => $params['name'],
	            'description' => $params['description']
	        ]);

	        $question_level->save();

	        $question_levels = $this->getAllQuestionLevels();

            return Rest::success(['$question_levels' => $question_levels, 'msg' => 'Question Level information was created successfully']);
        }
	}
	public function deleteQuestionLevel(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

	        Question_level::destroy($request->get('id'));

	        $question_levels = $this->getAllQuestionLevels();

            return Rest::success(['$question_levels' => $question_levels, 'msg' => 'Question Level information was deleted successfully']);
        }
	}

	public function updateQuestionLevel(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $additional = ['name' => 'required'];
            $validator = $this->questionLevelValidator($params, $additional);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $allowUpdate = false;        
	        $ifExist = Question_level::where('name' , $params['name'])->first();
	        if ($ifExist !== null) {
	            if ($params['id'] == $ifExist->id) {
	                $allowUpdate = true;
	            }
	        } else {
	            $allowUpdate = true;
	        }
	        	
	        if ($allowUpdate) {
	            $questionaire_group = Question_level::find($params['id']);
	            $questionaire_group->name = $params['name'];
	            $questionaire_group->description = $params['description'];
	            $questionaire_group->questionaire_group_id = $params['questionaire_group_id'];
	            $questionaire_group->save();
	        } else {
	        	return Rest::dataExist(['name' => ['The name has already been taken.']]);
	        }

	        $question_levels = $this->getAllQuestionLevels();

            return Rest::success(['$question_levels' => $question_levels, 'msg' => 'Question Level information was updated successfully']);
        }
	}
	/*---#Question-Level-------------------------------------------------------------------*/


	/*----Article-------------------------------------------------------------------*/
	public function getAllArticlesAjax(Request $request)
	{	
		$params = $request->all();
		$question_level_id = isset($params['question_level_id']) ? $params['question_level_id'] : '';
		return Rest::success(['$articles' => $this->getAllArticles($question_level_id)]);
	}

	protected function getAllArticles($question_level_id = '')
	{	
		if ($question_level_id === '') {
			return Article::orderBy('created_at', 'desc')->get()->toArray();
		} else {
			return Article::where('question_level_id', $question_level_id)->orderBy('created_at', 'desc')->get()->toArray();
		}
		
	}

	protected function articleValidator(array $data, $option = []) {
        $fields = [
        	'question_level_id' => 'required',
            'description' => 'min:3',
            'video_url' => 'min:3'
        ];
        return Validator::make($data , $fields + $option);
    }

	public function createArticle(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $additional = ['name' => 'required|unique:articles'];
            $validator = $this->articleValidator($params, $additional);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

	        $article = new Article([
	        	'question_level_id' => $params['question_level_id'],
	            'name' => $params['name'],
	            'description' => $params['description'],
	            'video_url' => $params['video_url']
	        ]);

	        $article->save();

	        $articles = $this->getAllArticles();

            return Rest::success(['$articles' => $articles, 'msg' => 'Article information was created successfully']);
        }
	}
	public function deleteArticle(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

	        Article::destroy($request->get('id'));

	        $articles = $this->getAllArticles();

            return Rest::success(['$articles' => $articles, 'msg' => 'Article information was deleted successfully']);
        }
	}

	public function updateArticle(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $additional = ['name' => 'required'];
            $validator = $this->articleValidator($params, $additional);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $allowUpdate = false;        
	        $ifExist = Article::where('name' , $params['name'])->first();
	        if ($ifExist !== null) {
	            if ($params['id'] == $ifExist->id) {
	                $allowUpdate = true;
	            }
	        } else {
	            $allowUpdate = true;
	        }
	        	
	        if ($allowUpdate) {
	            $article = Article::find($params['id']);
	            $article->name = $params['name'];
	            $article->description = $params['description'];
	            $article->question_level_id = $params['question_level_id'];
	            $article->video_url = $params['video_url'];
	            $article->save();
	        } else {
	        	return Rest::dataExist(['name' => ['The name has already been taken.']]);
	        }

	        $articles = $this->getAllArticles();

            return Rest::success(['$articles' => $articles, 'msg' => 'Article information was updated successfully']);
        }
	}
	/*---#Article-------------------------------------------------------------------*/

	
	/*----Questions-------------------------------------------------------------------*/
	public function getAllQuestionsAjax()
	{
		return Rest::success(['$questions' => $this->getAllQuestions()]);
	}

	protected function getAllQuestions()
	{
		$questions =  Question::orderBy('created_at', 'desc')->get()->toArray();

        foreach ($questions as $key => $value) {
    		$value['choices'] = (isset($value['choices'])) ? unserialize($value['choices']) : '[[{key : \'value\',value: \'\'},{key : \'label\', value: \'\'}]]';
    		$questions[$key] = $value;
        }
        return $questions;
	}

	protected function questionValidator(array $data, $option = []) {
        $fields = [
        	'question_level' => 'required',
        	'name' => 'required',
            'value_answer' => 'required',
            'type_field' => 'required',
            'model_name' => 'required',
            'status' => 'required'
        ];
        return Validator::make($data , $fields + $option);
    }

	public function createQuestion(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $validator = $this->questionValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $allowUpdate = false;        
	        $ifExist = Question::where('name' , $params['name'])->where('question_level_id' , $params['question_level'])->first();
	        if ($ifExist === null) {
	            $allowUpdate = true;
	        }
	        if ($allowUpdate) {
	           
	            $question = new Question([
		        	'article_id' => isset($params['article']) ? $params['article'] : -1,
		        	'question_level_id' => $params['question_level'],
		            'name' => $params['name'],
		            'value_answer' => $params['value_answer'],
		            'type_field' => $params['type_field'],
		            'choices' => (isset($params['choices'])) ? serialize($params['choices']) : 
		            	serialize('[{key : \'value\',value: \'\'},{key : \'label\', value: \'\'}]'),
		            'model_name' => $params['model_name'],
		            'status' => ($params['status'] === 'Enable') ? true : false
		        ]);

		        $question->save();

	        } else {
	        	return Rest::dataExist(['name' => ['The name has already been taken.']]);
	        }

	        $questions = $this->getAllQuestions();

            return Rest::success(['$questions' => $questions, 'msg' => 'Question information was created successfully']);
        }
	}
	public function deleteQuestion(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

	        Question::destroy($request->get('id'));

	        $questions = $this->getAllQuestions();

            return Rest::success(['$questions' => $questions, 'msg' => 'Question information was deleted successfully']);
        }
	}

	public function updateQuestion(Request $request) {
		if ($request->ajax()) { 
            $params = $request->all();

            $validator = $this->questionValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $allowUpdate = false;        
	        $ifExist = Question::where('name' , $params['name'])->where('question_level_id' , $params['question_level_id'])->first();
	        if ($ifExist !== null) {
	            if ($params['id'] == $ifExist->id) {
	                $allowUpdate = true;
	            }
	        } else {
	            $allowUpdate = true;
	        }

	        if ($allowUpdate) {
	            $question = Question::find($params['id']);
	            $question->article_id = isset($params['article']) ? $params['article'] : -1;
	            $question->question_level_id = $params['question_level'];
	            $question->name = $params['name'];
	            $question->value_answer = $params['value_answer'];
	            $question->type_field = $params['type_field'];
	            $question->choices = (isset($params['choices'])) ? serialize($params['choices']) : 
		            	serialize('[{key : \'value\',value: \'\'},{key : \'label\', value: \'\'}]');
		        $question->model_name = $params['model_name'];
		        $question->status = ($params['status'] === 'Enable') ? true : false;
	            $question->save();
	        } else {
	        	return Rest::dataExist(['name' => ['The name has already been taken.']]);
	        }

	        $questions = $this->getAllQuestions();

            return Rest::success(['$questions' => $questions, 'msg' => 'Question information was updated successfully']);
        }
	}
	/*---#Questions-------------------------------------------------------------------*/

	public function getAllQuestionnaires(Request $request)
	{
		if ($request->ajax())
		{
			$params = $request->all();

			$questionnaires = Questionnaire::with('pages')->orderBy('name', 'desc')->get()->toArray();

			return Rest::success(['$questionnaires' => $this->reconstructQuestionnaireArray($questionnaires)]);
		}
	}

	public function getQuestionnaireById(Request $request)
	{
		if ($request->ajax())
		{
			$params = $request->all();

			$questionnaire = Questionnaire::find($params['questionnaire_id'])->with('pages')->orderBy('name', 'desc')->get()->toArray();
			
			return Rest::success(['$questionnaire' => $this->reconstructQuestionnaireArray($questionnaire)]);
		}
	}

	protected function reconstructQuestionnaireArray($data = array())
	{
		$return_value = array();

		/*$data_class = new \stdClass;

		foreach ($data as $key => $questionnaire)
		{
			$page_class = new \stdClass;
			foreach ($questionnaire['pages'] as $key => $page)
			{
				$page_class->{$page['id']} = $page;

				
				$questionnaire['pages'][$page['id']]['page_items'] = unserialize($questionnaire['pages'][$page['id']]['page_items']);
				unset($questionnaire['pages'][$key]);
			}
			$data_class->{$questionnaire['id']} = $page_class;

			$questionnaire['pages']['length'] = count($questionnaire['pages']);
			$return_value[$questionnaire['id']] = $questionnaire;
		}
		$data_class = (array) $data_class;
		dd($data_class);
		exit;*/
		//dd($data);
		$questionnaire_array = array();
		$pages_array = array();
		foreach ($data as $key => $questionnaire)
		{
			foreach ($questionnaire['pages'] as $key => $page)
			{
				$pages_array[(string) $page['id']] = $page;
				$pages_array[$page['id']]['page_items'] = unserialize($pages_array[$page['id']]['page_items']);
				/*$questionnaire['pages'][(string) $page['id']] = $page;
				$questionnaire['pages'][(string) $page['id']]['page_items'] = unserialize($questionnaire['pages'][(string) $page['id']]['page_items']);*/
			}

			$questionnaire['criteria'] = unserialize($questionnaire['criteria']);
			$questionnaire_array[(string) $questionnaire['id']] = $questionnaire;
			$questionnaire_array[$questionnaire['id']]['pages'] = $pages_array;
			//$questionnaire['pages']['length'] = count($questionnaire['pages']);
			
			//unset($questionnaire['pages'][0]);

			//$return_value[(string) $questionnaire['id']] = $questionnaire;
		}

		return $questionnaire_array;
	}

	public function viewNotification(Request $request)
	{
		if ($request->ajax())
		{
			$params = $request->all();

			$notification = Notification::find($params['notification_id']);
			$notification->is_viewed = 1;

			$notification->save();

			$notifications = Notification::with('questionnaire', 'applicant')->orderBy('created_at', 'desc')->get()->toArray();
			
			foreach ($notifications as $key => $notification)
			{
				$mutator = new \Carbon\Carbon($notification['created_at']);
				$notifications[$key]['created_at'] = $mutator->diffForHumans();
			}

			return Rest::success(['$notifications' => $notifications]);
		}
	}

	public function getAllNotifications(Request $request)
	{
		if ($request->ajax())
		{
			$notifications = Notification::with('questionnaire', 'applicant')->orderBy('created_at', 'desc')->get()->toArray();
			
			foreach ($notifications as $key => $notification)
			{
				$mutator = new \Carbon\Carbon($notification['created_at']);
				$notifications[$key]['created_at'] = $mutator->diffForHumans();
			}

			return Rest::success(['$notifications' => $notifications]);
		}
	}

	public function createNewQuestionnaire(Request $request)
	{
		if ($request->ajax())
		{
			$params = $request->all();
			$validator = Validator::make($params, [
				'name' => 'required',
			]);

			if ($validator->fails())
				return Rest::failed($validator);

			$questionnaire = new Questionnaire([
				'name' => $params['name'],
				'color' => $params['color'],
				'criteria' => serialize($params['criteria']),
				'is_active' => ($params['is_active']) ? 1 : 0
			]);

			$questionnaire->save();

			return Rest::success([
				'$questionnaires' => $this->reconstructQuestionnaireArray(Questionnaire::with('pages')->orderBy('name', 'desc')->get()->toArray())
			]);
		}
	}

	public function createNewPage(Request $request)
	{
		if ($request->ajax())
		{
			$params = $request->all();

			$validator = Validator::make($params, [
				'questionnaire_id' => 'required',
			]);

			if ($validator->fails())
				return Rest::failed($validator);

			$page = new Questionnaire_page();
			$page->questionnaire_id = $params['questionnaire_id'];
			$page->page_items = serialize('[]');

			$page->save();

			$questionnaire = Questionnaire::find($params['questionnaire_id'])->with('pages')->orderBy('name', 'desc')->get()->toArray();

			return Rest::success(['$questionnaire' => $this->reconstructQuestionnaireArray($questionnaire), '$newPage' => $page->toArray()]);
		}
	}

	public function savePageChanges(Request $request)
	{
		if ($request->ajax())
		{
			$params = $request->all();

			$validator = Validator::make($params, [
				'page_items' => 'required',
				'page_id' => 'required'
			]);

			if ($validator->fails())
				return Rest::failed($validator);

			$page = Questionnaire_page::find($params['page_id']);
			$page->page_items = serialize($params['page_items']);

			$page->save();

			return Rest::success(['$page_items' => unserialize($page->page_items)]);
		}
	}

	public function deletePage(Request $request)
	{
		if ($request->ajax())
		{
			$params = $request->all();

			$validator = Validator::make($params, [
				'page_id' => 'required'
			]);

			if ($validator->fails())
				return Rest::failed($validator);

			$page = Questionnaire_page::find($params['page_id']);
			$page->delete();

			return Rest::success();
		}
	}

	public function getAllApplicantSubmission(Request $request) {
		if ($request->ajax())
		{
			$applicant_submission = Applicant_answer::with('applicants')->orderBy('created_at', 'desc')->get()->toArray();
			foreach ($applicant_submission as $key => $value) {
				$mutator = new \Carbon\Carbon($value['created_at']);
				$applicant_submission[$key]['created_at'] = $mutator->diffForHumans();
				$applicant_submission[$key]['result'] = unserialize($value['result']); 
				foreach ($applicant_submission[$key]['result'] as $k1 => $val) {
					foreach ($val['page_items'] as $k2 => $v) {
						if ($v['field_type'] === 'question') {
							$criteria = strtolower($v['criteria']);
							if (!array_key_exists($criteria, $applicant_submission[$key]['result'])) {
								if (!array_key_exists('list_criteria', $applicant_submission[$key]['result'])) {
									$applicant_submission[$key]['result']['list_criteria'] = [];
								}
								$applicant_submission[$key]['result']['list_criteria'][] = $criteria;
								$applicant_submission[$key]['result'][$criteria] = 0;
								$applicant_submission[$key]['result'][$criteria . '_total_number_question'] = 0;
								$questionnaire = Questionnaire::find($value['questionnaire_id'])->get()->toArray();
								$applicant_submission[$key]['result']['passing_criteria'] = unserialize($questionnaire[0]['criteria']);
							}
							$answers = $v['attributes']['answers'];
							$applicant_answer = $v['attributes']['applicant_answer'];
							$applicant_submission[$key]['result'][$criteria . '_total_number_question'] += 1;
							if (is_array($answers) && is_array($applicant_answer)) {
								if (count($answers) >= count($applicant_answer)) {
									$intersects = array_intersect($answers, $applicant_answer);
									if (count($intersects) === count($answers)) {
										$applicant_submission[$key]['result'][$criteria] += 1;
									}
								}
							} else {
								if (strtolower($answers) === strtolower($applicant_answer)) {
									$applicant_submission[$key]['result'][$criteria] += 1;
								}
							}

						}
					}
				}
			}
			return Rest::success(['$applicant_submission' => $applicant_submission]);
		}
	}

	public function getAllQuestionnairesByApplicantId(Request $request) {
		if ($request->ajax()) {
			$params = $request->all();
			$applicant = Applicant_answer::where('applicant_id', $params['applicant_id'])->get()->toArray();
			$applicant_questionnaire_results = [];
			foreach ($applicant as $key => $value) {
				$applicant_questionnaire_results = unserialize($value['result']);
			}
			return Rest::success(['$questionnaire_page' => $applicant_questionnaire_results]);
		} 
	}

	public function getGradeResultsByApplicantId(Request $request) {
		if ($request->ajax()) {
			$params = $request->all();
			$applicant = Applicant_answer::where('applicant_id', $params['applicant_id'])->with('applicants')->get()->toArray();
			$applicant_questionnaire_results = [];
			$applicant[0]['result'] = unserialize($applicant[0]['result']); 
			foreach ($applicant[0]['result'] as $k1 => $val) {
				foreach ($val['page_items'] as $k2 => $v) {
					if ($v['field_type'] === 'question') {
						$criteria = strtolower($v['criteria']);
						if (!array_key_exists($criteria, $applicant[0]['result'])) {
							if (!array_key_exists('list_criteria', $applicant[0]['result'])) {
								$applicant[0]['result']['list_criteria'] = [];
							}
							$applicant[0]['result']['list_criteria'][] = $criteria;
							$applicant[0]['result'][$criteria] = 0;
							$applicant[0]['result'][$criteria . '_total_number_question'] = 0;
							$questionnaire = Questionnaire::find($applicant[0]['questionnaire_id'])->get()->toArray();
							$applicant[0]['result']['passing_criteria'] = unserialize($questionnaire[0]['criteria']);
							$applicant[0]['questionnaire_name'] = $questionnaire[0]['name'];
						}
						$answers = $v['attributes']['answers'];
						$applicant_answer = $v['attributes']['applicant_answer'];
						$applicant[0]['result'][$criteria . '_total_number_question'] += 1;
						if (is_array($answers) && is_array($applicant_answer)) {
							if (count($answers) >= count($applicant_answer)) {
								$intersects = array_intersect($answers, $applicant_answer);
								if (count($intersects) === count($answers)) {
									$applicant[0]['result'][$criteria] += 1;
								}
							}
						} else {
							if (strtolower($answers) === strtolower($applicant_answer)) {
								$applicant[0]['result'][$criteria] += 1;
							}
						}

					}
				}
			}
		
			return Rest::success(['$applicant_results' => $applicant[0] ]);
		}
	}
}	