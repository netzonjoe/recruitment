<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model {

	protected $table = 'applicants';

	public $timestamps = true;
	
	protected $fillable = [
		'firstname',
		'lastname',
		'middlename',
		'gender',
		'contact_number',
		'date_of_birth',
		'birth_place',
		'present_address',
		'permanent_address',
		'civil_status',
		'email',
		'position_applied'
	];
	
	protected $visible = ['firstname',
		'lastname',
		'middlename',
		'gender',
		'contact_number',
		'date_of_birth',
		'birth_place',
		'present_address',
		'permanent_address',
		'civil_status',
		'email',
		'position_applied'];


}