<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field_Group extends Model {

	protected $table = 'field_groups';

	public $timestamps = true;
	
	protected $fillable = array('name', 'icon');
}