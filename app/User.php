<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{   

    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'user_group_id', 'status', 'email', 'password',
    ];

    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    * Get the User_group that owns the User.
    */
    public function userGroup()
    {
        return $this->belongsTo('App\User_group');
    }

    /**
     * Get the the Additional Info for the User.
     */
    public function additionalInfo()
    {
        return $this->hasMany('App\Additional_info');
    }

   
}
