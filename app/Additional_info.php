<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Additional_info extends Model {

	protected $table = 'additional_infos';
	
    public $timestamps = true;

    protected $fillable = ['key', 'value'];

    protected $dates = ['created_at', 'updated_at'];

    public static $keys = [
        'applicant'
    ];

	/**
    * Get the Job that owns the User.
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}