<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Custom_field extends Model {

	protected $table = 'custom_fields';
	
	public $timestamps = true;

	protected $fillable = [
		'field_group_id',
		'model_name',
		'label',
		'placeholder',
		'type_field',
		'is_required',
		'choices',
		'status'
	];

	protected $dates = ['created_at', 'updated_at'];

}