<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

	protected $table = 'notifications';

	public $timestamps = true;
	
	protected $fillable = array('applicants_id', 'questionnaire_id');

	public function questionnaire()
	{
		return $this->belongsTo('App\Questionnaire', 'questionnaire_id');
	}

	public function applicant()
	{
		return $this->belongsTo('App\Applicant', 'applicants_id');
	}
}