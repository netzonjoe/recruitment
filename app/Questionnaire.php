<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model {

	protected $table = 'questionnaires';

	protected $fillable = ['name', 'color', 'is_active', 'criteria'];

	protected $dates = ['created_at', 'updated_at'];

	public function pages()
	{
		return $this->hasMany('App\Questionnaire_page');
	}
}