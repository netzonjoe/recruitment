<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire_page extends Model {

	protected $table = 'questionnaire_pages';

	protected $fillable = [
		'questionnaire_id',
		'page_sequence',
		'page_items'
	];

	protected $dates = ['created_at', 'updated_at'];
}