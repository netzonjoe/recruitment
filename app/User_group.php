<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_group extends Model {

	protected $table = 'user_groups';
	public $timestamps = true;
	protected $fillable = ['name', 'description'];

	/**
     * Get the the User details for the User_group.
     */
    public function userDetails()
    {
        return $this->hasMany('App\User');
    }
}