'use strict';
var recruitment = angular.module('RecruitmentControllers', []);


recruitment.controller('RecruitmentCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {
}]);

recruitment.controller('Employment_informationCtrl', ['$scope', 'http', 'validator', 'global', '$location',function ($scope, http, validator, global, $location) {

	var resetFormData = function () {
		$scope.data = {
		    firstname: '',
		    lastname: '',
		    middlename: '',
		    contact_number: '',
		    email: '',
		    birth_place: '',
            present_address: '',
            permanent_address: '',
            position_applied: ''
		};
		$('.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();
    $scope.$questionnaires = [];
    http.getPositionsAppliedAjax({}).then(function (data) {
        $scope.$questionnaires = angular.copy(data.data.$questionnaires);
    });

	$scope.today = function() {
        $scope.data.date_of_birth = new Date();
    };
    $scope.today();

    $scope.clear = function() {
        $scope.data.date_of_birth = null;
    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();
    $scope.maxDate = new Date(2020, 5, 22);

    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function(year, month, day) {
        $scope.$apply(function () {
            $scope.data.date_of_birth = new Date(year, month, day);
        });
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['MMMM dd, yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);

    $scope.$steps = [];
    var current_step_index = [];
    var current_index = 0;

    current_step_index[0] = 0;

    $scope.$steps[0] = {key: 'Personal Information', is_active: false};

    http.getAllFieldGroupsAjax({});
    global.field_groups.listTrigger(function (field_groups) {
        $scope.$field_groups = angular.copy(field_groups);
        console.log($scope.$field_groups);
        $scope.$field_groups.forEach(function (value, group_key) {
            $scope.$steps[value.id] = {key: value.name, is_active: false, group_key: group_key};
            current_step_index.push(value.id);
        });

        current_step(current_index);
    });

    var resetSteps = function () {
        $scope.$steps.forEach(function(value) {
            value.is_active = false;
        });
    }
    
    var current_step = function (index) {
        resetSteps();
        $scope.$steps[current_step_index[index]].is_active = true;
    }

    $scope.current_step = function () {
        current_index -= 1;
        current_step(current_index);
    }
    $scope.onCustomFieldSubmit = function (field_group_id) {
        $scope.$field_groups[$scope.$steps[field_group_id].group_key].fields.lastStep = false;

        if (current_index == (current_step_index.length - 1))
        {
            $scope.$field_groups[$scope.$steps[field_group_id].group_key].fields.lastStep = true;
        }

        http.onCustomFieldSubmit($scope.$field_groups[$scope.$steps[field_group_id].group_key].fields).then(function (data) {
            $scope.errors = {};
            console.log($scope.$field_groups[$scope.$steps[field_group_id].group_key].fields);
            if ($scope.$field_groups[$scope.$steps[field_group_id].group_key].fields.lastStep)
            {   
                http.saveEmployeeInformation({}).then(function (data) {
                    $location.path('/questionnaire')
                });
                
                return;
            }
            current_index += 1;
            current_step(current_index);
            
        }, function (data) {
            if (data.errors) {
                $scope.errors = data.errors;
            }
        });
    }

    $scope.onSavePersonalInfo = function() {
    	http.savePersonalInfo($scope.data).then(function (data) {
            $scope.data = data.data.$personal_info;
			$scope.errors = {};
            current_index += 1;
            current_step(current_index);
		}, function (data) {
			if (data.errors) {
				$scope.errors = data.errors;
			}
		});
    }
}]);

recruitment.controller('QuestionnaireCtrl', ['$scope', 'http', 'validator', 'global','$window', '$location', function ($scope, http, validator, global, $window, $location) {
    http.getAllQuestionnaires({});
    global.questionnaire_page.listTrigger(function (questionnaire_page) {
        if (questionnaire_page.length) {
            $scope.$questionnaire_page = angular.copy(questionnaire_page);
            console.log($scope.$questionnaire_page);
        } else {
            location.href = location.href.substring(0, location.href.indexOf('#'));
        }
    }, function () {
        location.href = location.href.substring(0, location.href.indexOf('#'));
    });

    $scope.page_number = 0;
    $scope.turnPageTo = function (page_number) {console.log($scope.$questionnaire_page);
        if (page_number >= 0 && page_number < $scope.$questionnaire_page.length)
            $scope.page_number = page_number;
    };

    $scope.onSubmitForm = function () {
        http.submitAllAnswers({data: $scope.$questionnaire_page}).then(function (data) {
            if (data.data.redirect) {
                location.href = location.href.substring(0, location.href.indexOf('#'));
            }
        }, function (error) {
            location.href = location.href.substring(0, location.href.indexOf('#'));
        });
    };

}]);