'use strict';

var recruitment = angular.module('RecruitmentServices', []);

recruitment.factory('http', ['$http', '$q', 'global', 'cfpLoadingBar', function($http, $q, global, cfpLoadingBar) {
	return {
		alert: {
			type: 'success',
            msg: 'Product information was updated successfully'
		},
		_token: $_token,
		post: function (url, data) {
			var self = this;
			cfpLoadingBar.start();
			return $q(function(resolve, reject) {
				data._token = self._token; 
		        jQuery.ajax({
		        	cache: false,
		        	type: 'POST',
		        	dataType: 'json',
		        	url: $route_list['POST'][url],
				    data: data,
				    headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
				   	success: function(data) {
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	resolve({
				   			data:data,
							status:200,
							statusText:"OK"	
				   		});
				   		cfpLoadingBar.complete();
				   	},
				   	error: function(data) {
				   		console.log(data);
				   		var data = JSON.parse(data.responseText);

				   		if (data.errors.type !== undefined && data.errors.msg !== undefined) {
				   			self.alert.type = data.errors.type;	
				   			self.alert.msg = data.errors.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	reject(data);
				      	cfpLoadingBar.complete();
				   	}
		        });
  			});
		},
		postAndUpload: function (url, data) {
			var self = this;
			cfpLoadingBar.start();
			return $q(function(resolve, reject) {
				data._token = self._token; 
		        jQuery.ajax({
		        	type: 'POST',
		        	dataType: 'json',
		        	url: $route_list['POST'][url],
				    data: data,
				    headers: {
				        'X-CSRF-TOKEN': self._token
				    },
				    async: false,
					cache: false,
					contentType: false,
					processData: false,
				   	success: function(data) {
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	resolve({
				   			data:data,
							status:200,
							statusText:"OK"	
				   		});
				   		cfpLoadingBar.complete();
				   	},
				   	error: function(data) {
				   		var data = JSON.parse(data.responseText);
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	reject(data);
				      	cfpLoadingBar.complete();
				   	}
		        });
  			});
		},
		savePersonalInfo: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('GuestController@savePersonalInfo', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		onCustomFieldSubmit: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('GuestController@saveCustomFieldInfo', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllFieldGroupsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('GuestController@getAllFieldGroupsAjax', data).then(function(response) {
					if (response.data.$field_groups !== undefined) {
			   			global.field_groups.listUpdate(response.data.$field_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getPositionsAppliedAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('GuestController@getPositionsAppliedAjax', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		saveEmployeeInformation: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('GuestController@saveEmployeeInformation', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		
		submitAllAnswers: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('GuestController@submitAllAnswers', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllQuestionnaires: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('GuestController@getAllQuestionnaires', data).then(function(response) {
					if (response.data.$questionnaire_page !== undefined) {
						var $questionnaire_page = response.data.$questionnaire_page;
						for (var i in $questionnaire_page) {
							var page_items = JSON.parse($questionnaire_page[i].page_items);
							var _page_items = [];
							for (var j in page_items) {
								if (page_items[j] !== null) {
									if (page_items[j].attributes['choices']['choice_type'] === 'MULTIPLE SELECTION') {
										page_items[j].attributes['applicant_answer'] = [];
									} else {
										page_items[j].attributes['applicant_answer'] = '';
									}
									_page_items.push(page_items[j]);
								}
							}
							if (page_items.length > 0) {
								$questionnaire_page[i].page_items = _page_items;
							} else {
								delete $questionnaire_page[i];
							}
						}

						var _questionnaire_page = [];
						for (var i in $questionnaire_page) {
							if ($questionnaire_page[i] !== undefined) {
								_questionnaire_page.push($questionnaire_page[i]);
							}
						}
			   			global.questionnaire_page.listUpdate(_questionnaire_page);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		}
	};
}]);

recruitment.factory('global', [function() {
	return {
		alert: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		users: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		users_counter: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		field_groups: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		questionnaire_page: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		}

	};
}]);

recruitment.factory('validator', [function() {
	return {
		errors: {},
		hasError: function (key, classError) {
			return (this.errors[key] === undefined) ? '' : classError;
		},
		showErrorBlock: function (key, classError) {
			return (this.errors[key] === undefined) ? classError : '';
		}
	};
}]);

recruitment.factory('generator', [function() {
	return {
		reference: function () {
			var first3Char = "";
		    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		    for( var i=0; i < 3; i++ )
		        first3Char += possible.charAt(Math.floor(Math.random() * possible.length));

		    return first3Char + Math.round(new Date().getTime()).toString().substring(6);
		}
	};
}]);


