(function () {
    'use strict';

    String.prototype.toController = function() {
        return this.charAt(0).toUpperCase() + this.slice(1) + 'Ctrl';
    };
    angular.module('app')
        .config(['$routeProvider', function($routeProvider) {
            var routes, setRoutes;

            routes = [
                'employment_information',
                'questionnaire'
            ]

            setRoutes = function(route) {
                var config, url;
                url = '/' + route;
                config = {
                    templateUrl: 'templates/guest-dashboard/' + route + '.html',
                    controller: route.toController()
                };
                $routeProvider.when(url, config);
                return $routeProvider;
            };

            routes.forEach(function(route) {
                return setRoutes(route);
            });

            $routeProvider
                .when('/', {redirectTo: '/employment_information'})
                .when('/404', {templateUrl: 'templates/guest-dashboard/page/404.html'})
                .otherwise({ redirectTo: '/404'});

        }]
    );

})(); 