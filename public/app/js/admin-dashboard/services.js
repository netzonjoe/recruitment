'use strict';

var recruitment = angular.module('RecruitmentServices', []);

recruitment.factory('http', ['$http', '$q', 'global', 'cfpLoadingBar', function($http, $q, global, cfpLoadingBar) {
	return {
		alert: {
			type: 'success',
            msg: 'Product information was updated successfully'
		},
		_token: $_token,
		post: function (url, data) {
			var self = this;
			cfpLoadingBar.start();
			return $q(function(resolve, reject) {
				data._token = self._token; 
		        jQuery.ajax({
		        	cache: false,
		        	type: 'POST',
		        	dataType: 'json',
		        	url: $route_list['POST'][url],
				    data: data,
				    headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
				   	success: function(data) {
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	resolve({
				   			data:data,
							status:200,
							statusText:"OK"	
				   		});
				   		cfpLoadingBar.complete();
				   	},
				   	error: function(data) {
				   		var data = JSON.parse(data.responseText);

				   		if (data.errors.type !== undefined && data.errors.msg !== undefined) {
				   			self.alert.type = data.errors.type;	
				   			self.alert.msg = data.errors.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	reject(data);
				      	cfpLoadingBar.complete();
				   	}
		        });
  			});
		},
		postAndUpload: function (url, data) {
			var self = this;
			cfpLoadingBar.start();
			return $q(function(resolve, reject) {
				data._token = self._token; 
		        jQuery.ajax({
		        	type: 'POST',
		        	dataType: 'json',
		        	url: $route_list['POST'][url],
				    data: data,
				    headers: {
				        'X-CSRF-TOKEN': self._token
				    },
				    async: false,
					cache: false,
					contentType: false,
					processData: false,
				   	success: function(data) {
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	resolve({
				   			data:data,
							status:200,
							statusText:"OK"	
				   		});
				   		cfpLoadingBar.complete();
				   	},
				   	error: function(data) {
				   		var data = JSON.parse(data.responseText);
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	reject(data);
				      	cfpLoadingBar.complete();
				   	}
		        });
  			});
		},
		getAllUserGroupsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllUserGroupsAjax', data).then(function(response) {
					if (response.data.$user_groups !== undefined) {
			   			global.user_groups.listUpdate(response.data.$user_groups);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});	
		},
		createUserGroup: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createUserGroup', data).then(function(response) {
					if (response.data.$user_groups !== undefined) {
			   			global.user_groups.listUpdate(response.data.$user_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
			
		},
		deleteUserGroup: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteUserGroup', data).then(function(response) {
					if (response.data.$user_groups !== undefined) {
			   			global.user_groups.listUpdate(response.data.$user_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
			
		},
		updateUserGroup: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateUserGroup', data).then(function(response) {
					if (response.data.$user_groups !== undefined) {
			   			global.user_groups.listUpdate(response.data.$user_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
			
		},
		getAllUsersAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllUsersAjax', data).then(function(response) {
					if (response.data.$users !== undefined) {
			   			global.users.listUpdate(response.data.$users);	
			   		}
			   		if (response.data.$users_counter !== undefined) {
			   			global.users_counter.listUpdate(response.data.$users_counter);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
			
		},
		createUser: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createUser', data).then(function(response) {
					if (response.data.$users !== undefined) {
			   			global.users.listUpdate(response.data.$users);	
			   		}
			   		if (response.data.$users_counter !== undefined) {
			   			global.users_counter.listUpdate(response.data.$users_counter);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
			
		},
		deleteUser: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteUser', data).then(function(response) {
					if (response.data.$users !== undefined) {
			   			global.users.listUpdate(response.data.$users);	
			   		}
			   		if (response.data.$users_counter !== undefined) {
			   			global.users_counter.listUpdate(response.data.$users_counter);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
			
		},
		updateUser: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateUser', data).then(function(response) {
					if (response.data.$users !== undefined) {
			   			global.users.listUpdate(response.data.$users);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createField: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createField', data).then(function(response) {
					if (response.data.$fields !== undefined) {
			   			global.fields.listUpdate(response.data.$fields);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateField: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateField', data).then(function(response) {
					if (response.data.$fields !== undefined) {
			   			global.fields.listUpdate(response.data.$fields);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteField: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteField', data).then(function(response) {
					if (response.data.$fields !== undefined) {
			   			global.fields.listUpdate(response.data.$fields);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllFieldsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllFieldsAjax', data).then(function(response) {
					if (response.data.$fields !== undefined) {
			   			global.fields.listUpdate(response.data.$fields);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createFieldGroup: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createFieldGroup', data).then(function(response) {
					if (response.data.$field_groups !== undefined) {
			   			global.field_groups.listUpdate(response.data.$field_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllFieldGroupsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllFieldGroupsAjax', data).then(function(response) {
					if (response.data.$field_groups !== undefined) {
			   			global.field_groups.listUpdate(response.data.$field_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteFieldGroup: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteFieldGroup', data).then(function(response) {
					if (response.data.$field_groups !== undefined) {
			   			global.field_groups.listUpdate(response.data.$field_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateFieldGroup: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateFieldGroup', data).then(function(response) {
					if (response.data.$field_groups !== undefined) {
			   			global.field_groups.listUpdate(response.data.$field_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllQuestionaireGroupsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllQuestionaireGroupsAjax', data).then(function(response) {
					if (response.data.$questionaire_groups !== undefined) {
			   			global.questionaire_groups.listUpdate(response.data.$questionaire_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createQuestionaireGroup: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createQuestionaireGroup', data).then(function(response) {
					if (response.data.$questionaire_groups !== undefined) {
			   			global.questionaire_groups.listUpdate(response.data.$questionaire_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateQuestionaireGroup: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateQuestionaireGroup', data).then(function(response) {
					if (response.data.$questionaire_groups !== undefined) {
			   			global.questionaire_groups.listUpdate(response.data.$questionaire_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteQuestionaireGroup: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteQuestionaireGroup', data).then(function(response) {
					if (response.data.$questionaire_groups !== undefined) {
			   			global.questionaire_groups.listUpdate(response.data.$questionaire_groups);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllQuestionLevelsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllQuestionLevelsAjax', data).then(function(response) {
					if (response.data.$question_levels !== undefined) {
			   			global.question_levels.listUpdate(response.data.$question_levels);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createQuestionLevel: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createQuestionLevel', data).then(function(response) {
					if (response.data.$question_levels !== undefined) {
			   			global.question_levels.listUpdate(response.data.$question_levels);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateQuestionLevel: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateQuestionLevel', data).then(function(response) {
					if (response.data.$question_levels !== undefined) {
			   			global.question_levels.listUpdate(response.data.$question_levels);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteQuestionLevel: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteQuestionLevel', data).then(function(response) {
					if (response.data.$question_levels !== undefined) {
			   			global.question_levels.listUpdate(response.data.$question_levels);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllArticlesAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllArticlesAjax', data).then(function(response) {
					if (response.data.$articles !== undefined) {
			   			global.articles.listUpdate(response.data.$articles);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createArticle: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createArticle', data).then(function(response) {
					if (response.data.$articles !== undefined) {
			   			global.articles.listUpdate(response.data.$articles);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateArticle: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateArticle', data).then(function(response) {
					if (response.data.$articles !== undefined) {
			   			global.articles.listUpdate(response.data.$articles);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteArticle: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteArticle', data).then(function(response) {
					if (response.data.$articles !== undefined) {
			   			global.articles.listUpdate(response.data.$articles);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllQuestionsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllQuestionsAjax', data).then(function(response) {
					if (response.data.$questions !== undefined) {
			   			global.questions.listUpdate(response.data.$questions);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createQuestion: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createQuestion', data).then(function(response) {
					if (response.data.$questions !== undefined) {
			   			global.questions.listUpdate(response.data.$questions);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateQuestion: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateQuestion', data).then(function(response) {
					if (response.data.$questions !== undefined) {
			   			global.questions.listUpdate(response.data.$questions);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteQuestion: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteQuestion', data).then(function(response) {
					if (response.data.$questions !== undefined) {
			   			global.questions.listUpdate(response.data.$questions);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getQuestionnaires: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllQuestionnaires', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getQuestionnaire: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getQuestionnaireById', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createNewQuestionnaire: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createNewQuestionnaire', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createNewPage: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createNewPage', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		savePageChanges: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@savePageChanges', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deletePage: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deletePage', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllApplicantSubmission: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllApplicantSubmission', data).then(function(response) {
					if (response.data.$applicant_submission !== undefined) {
			   			global.applicant_submission.listUpdate(response.data.$applicant_submission);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllQuestionnairesByApplicantId: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllQuestionnairesByApplicantId', data).then(function(response) {
					if (response.data.$questionnaire_page !== undefined) {
			   			global.questionnaire_page.listUpdate(response.data.$questionnaire_page);
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllNotifications: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllNotifications', data).then(function(response) {
					if (response.data.$notifications !== undefined) {
			   			global.notifications.listUpdate(response.data.$notifications);
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		viewNotification: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@viewNotification', data).then(function(response) {
					if (response.data.$notifications !== undefined) {
			   			global.notifications.listUpdate(response.data.$notifications);
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getGradeResultsByApplicantId: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getGradeResultsByApplicantId', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
	};
}]);

recruitment.factory('global', [function() {
	return {
		alert: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		users: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		users_counter: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		user_groups: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		field_groups: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		fields:{
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		questionaire_groups: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		question_levels: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		articles: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		questions: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		applicant_submission: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		questionnaire_page: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		notifications: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		}
	};
}]);

recruitment.factory('validator', [function() {
	return {
		errors: {},
		hasError: function (key, classError) {
			return (this.errors[key] === undefined) ? '' : classError;
		},
		showErrorBlock: function (key, classError) {
			return (this.errors[key] === undefined) ? classError : '';
		}
	};
}]);

recruitment.factory('generator', [function() {
	return {
		reference: function () {
			var first3Char = "";
		    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		    for( var i=0; i < 3; i++ )
		        first3Char += possible.charAt(Math.floor(Math.random() * possible.length));

		    return first3Char + Math.round(new Date().getTime()).toString().substring(6);
		}
	};
}]);


