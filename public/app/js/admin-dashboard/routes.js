(function () {
    'use strict';

    String.prototype.toController = function() {
        return this.charAt(0).toUpperCase() + this.slice(1) + 'Ctrl';
    };
    angular.module('app')
        .config(['$routeProvider', function($routeProvider) {
            var routes, setRoutes;

            routes = [
                'dashboard',
                'user_management',
                'user_groups',
                'field_groups',
                'fields',
                'field_assign',
                'questionaire_groups',
                'question_levels',
                'articles',
                'questions',
                'questionnaire_types',
                'questionnaire_pages',
                'grades'
            ]

            $routeProvider.when(
                '/questionnaire_pages_preview/:questionnaire_id/',
                {
                    templateUrl: 'templates/admin-dashboard/questionnaire_pages_preview.html',
                    controller: 'QuestionnairePagesController'
                }
            );

            $routeProvider.when(
                '/results_comparison/:applicant_id/',
                {
                    templateUrl: 'templates/admin-dashboard/results_comparison.html',
                    controller: 'Results_comparisonCtrl'
                }
            );

            $routeProvider.when(
                '/grade_results/:applicant_id/',
                {
                    templateUrl: 'templates/admin-dashboard/grade_results.html',
                    controller: 'Grade_resultsCtrl'
                }
            );

            setRoutes = function(route) {
                var config, url;
                url = '/' + route;
                config = {
                    templateUrl: 'templates/admin-dashboard/' + route + '.html',
                    controller: route.toController()
                };
                $routeProvider.when(url, config);
                return $routeProvider;
            };

            routes.forEach(function(route) {
                return setRoutes(route);
            });

            $routeProvider
                .when('/', {redirectTo: '/dashboard'})
                .when('/404', {templateUrl: 'templates/admin-dashboard/page/404.html'})
                .otherwise({ redirectTo: '/404'});
        }]
    );

})(); 