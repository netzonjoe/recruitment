'use strict';

var recruitment = angular.module('RecruitmentDirectives', []);

recruitment.directive('phoneInput', function($filter, $browser) {
    return {
        require: 'ngModel',
        link: function($scope, $element, $attrs, ngModelCtrl) {
            var listener = function() {
                var value = $element.val().replace(/[^0-9]/g, '');
                $element.val($filter('tel')(value, false));
            };

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function(viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function() {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function() {
                $browser.defer(listener);
            });
        }

    };
});
recruitment.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;

            default:
                city = value.slice(0, 3);
                number = value.slice(3);
        }

        if(number){
            if(number.length>3){
                number = number.slice(0, 3) + '-' + number.slice(3,7);
            }
            else{
                number = number;
            }

            return ("(" + city + ") " + number).trim();
        }
        else{
            return "(" + city;
        }

    };
});

recruitment.directive('selectPicker', function($timeout){
    return {
      restrict: 'A',
      link:function(scope, elem){
        $timeout(function() {
          elem.selectpicker({showSubtext:true});
          elem.selectpicker('refresh');
        }, 0);
      }
    };
});

recruitment.directive('checkboxPicker', function(){
    return {
      restrict: 'A',
      link:function(scope, elem){
        elem.click(function () {
            jQuery('input[name*=\'selected\']').prop('checked', elem[0].checked);
        });
      }
    };
});

recruitment.directive('restrict', function($parse){
    return {
      restrict: 'A',
      require: 'ngModel',
        link: function(scope, iElement, iAttrs, controller) {
            scope.$watch(iAttrs.ngModel, function(value) {
                if (!value) {
                    return;
                }
                $parse(iAttrs.ngModel).assign(scope, value.toUpperCase().replace(new RegExp(iAttrs.restrict, 'g'), '').replace(/\s+/g, '-'));
            });
        }
    };
});

recruitment.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }]);

recruitment.directive('fileImageSrc', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       scope: {
            fileImageSrc: '='
       },
       link: function(scope, element, attrs) {
          // var model = $parse(attrs.fileImageModel);
          // var modelSetter = model.assign;
          element.bind('change', function(){
             scope.$apply(function(){
                // modelSetter(scope, element[0].files[0]);
                if (element[0].files[0]) {
                    var FR = new FileReader();
                    FR.onload = function(e) {
                        scope.$apply(function () {
                            scope.fileImageSrc = e.target.result;
                        });
                    };       
                    FR.readAsDataURL( element[0].files[0] );
                }
             });

          });
       }
    };
 }]);

recruitment.directive('onReadyState', function () {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          element[0].style.display = "block";
       }
    };
 });


recruitment.directive('tableRow', function ($compile, $timeout) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            rows: "=",
            key: "=",
            index: "=",
            length: "=",
            icons: "=",
            onRemoveRow: "&"
        },
        link: function (scope, element, attrs) {
            var tableRowHtml;
            if (scope.key === 'choices') {
              tableRowHtml  = 
                  '<tr ng-repeat="row in rows"> ' +
                  '    <td ng-if="$index == 0" rowspan="2" class="side-background">{{index + 1}}</td>' +
                  '    <td ng-if="$index == 0"> Value </td> ' +
                  '    <td ng-if="$index == 0"> ' + 

                  '       <input type="text" class="form-control" ' +
                  '                placeholder="" required  readonly="readonly"' +
                  '                data-ng-minlength=1 ng-model="rows[0].value" > </td> ' +
                  /*--------------------------------------------------------------------*/
                  '    <td ng-if="$index == 1"> Label </td> ' +
                  '    <td ng-if="$index == 1"> ' + 
                  '       <input type="text" class="form-control" ' +
                  '                placeholder="" required ' +
                  '                data-ng-minlength=1 ng-model="rows[1].value"> </td> ' +
                  /*--------------------------------------------------------------------*/
                  '    <td ng-if="$index == 0" rowspan="2" class="side-background"> ' +
                  '        &nbsp;&nbsp; ' +
                  '        <div class="position-relative"> ' +
                  '            <div class="form-group btn-icon-round-minus"> ' +
                  '                <a href="javascript:void(0);" ' +
                  '                    ng-click="onRemoveRowItem(index)" ' +
                  '                    class="btn-icon-lined btn-icon-round btn-icon-sm gray"> ' +
                  '                    <span class="ti-minus"></span> ' +
                  '                </a> ' +
                  '            </div> ' +
                  '        </div> ' +
                  '    </td> ' +
                  '</tr>';
                  
            }
            
            scope.$watch(function(value) {
                scope.label = angular.copy(scope.rows[1].value);
            });

            scope.$watch('label',function(value) {
                if (value !== undefined) {
                    scope.rows[0].value = scope.label.toUpperCase().replace(new RegExp('[^a-zA-Z0-9\\-\\s]', 'g'), '').replace(/\s+/g, '-');
                } else {
                    scope.rows[0].value = '';
                }
            });
            $('#' + scope.key).append($compile(tableRowHtml)(scope));

            scope.onRemoveRowItem = function (index) {
                if (scope.length > 1) {
                    $('#' + scope.key).html('');
                    scope.onRemoveRow({index: index});
                }
            };
        }
    };
 });

recruitment.directive('tableRowCriteria', function ($compile, $timeout) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            rows: "=",
            key: "=",
            index: "=",
            length: "=",
            icons: "=",
            onRemoveRow: "&"
        },
        link: function (scope, element, attrs) {
            var  tableRowHtml  = 
                  '<tr ng-repeat="row in rows"> ' +
                  '    <td ng-if="$index == 0" rowspan="2" class="side-background">{{index + 1}}</td>' +
                  '    <td ng-if="$index == 0"> Name </td> ' +
                  '    <td ng-if="$index == 0"> ' + 

                  '       <input type="text" class="form-control" ' +
                  '                placeholder="" required' +
                  '                data-ng-minlength=1 ng-model="rows[0].value" > </td> ' +
                  /*--------------------------------------------------------------------*/
                  '    <td ng-if="$index == 1"> Passing </td> ' +
                  '    <td ng-if="$index == 1"> ' + 
                  '       <input type="text" class="form-control" ' +
                  '                placeholder="" required ' +
                  '                data-ng-minlength=1 ng-model="rows[1].value"> </td> ' +
                  /*--------------------------------------------------------------------*/
                  '    <td ng-if="$index == 0" rowspan="2" class="side-background"> ' +
                  '        &nbsp;&nbsp; ' +
                  '        <div class="position-relative"> ' +
                  '            <div class="form-group btn-icon-round-minus"> ' +
                  '                <a href="javascript:void(0);" ' +
                  '                    ng-click="onRemoveRowItem(index)" ' +
                  '                    class="btn-icon-lined btn-icon-round btn-icon-sm gray"> ' +
                  '                    <span class="ti-minus"></span> ' +
                  '                </a> ' +
                  '            </div> ' +
                  '        </div> ' +
                  '    </td> ' +
                  '</tr>';

            $('#' + scope.key).append($compile(tableRowHtml)(scope));

            scope.onRemoveRowItem = function (index) {
                if (scope.length > 1) {
                    $('#' + scope.key).html('');
                    scope.onRemoveRow({index: index});
                }
            };
        }
    };
 });

recruitment.directive('iconPicker', function ($timeout) {
    return {
       restrict: 'A',
       scpoe: {
          onUpdateIcon: "&"
       },
       link: function(scope, element, attrs) {
          var self = {
            scope : scope,
            element: element, 
            attrs: attrs
          };

          scope.onChangeValueInput= function (icon) {
              scope.onUpdateIcon({icon: icon});
          };
          element.iconpicker().iconpicker('setIconset', $.iconset_fontawesome)
                .bind('change', $.proxy(function (e) {
              this.onChangeValueInput(e.target.getElementsByTagName('i')[0].getAttribute('class'));
           },scope));
       }
    };
 });

/*recruitment.directive('radiochecked', function(){
    return {
      restrict: 'A',
      scope: {
            isChecked: '='
      },
      link:function(scope, elem){
        if (scope.isChecked)
        {
          jQuery(elem).prop('checked', true);
          console.log(jQuery(elem));
          console.log(elem[0]);
          elem.prop("checked", true).checkboxradio('refresh');
        }
      }
    };
});*/


/**
 * Checklist-model
 * AngularJS directive for list of checkboxes
 * https://github.com/vitalets/checklist-model
 * License: MIT http://opensource.org/licenses/MIT
 */

recruitment.directive('checklistModel', ['$parse', '$compile', function($parse, $compile) {
  // contains
  function contains(arr, item, comparator) {
    if (angular.isArray(arr)) {
      for (var i = arr.length; i--;) {
        if (comparator(arr[i], item)) {
          return true;
        }
      }
    }
    return false;
  }

  // add
  function add(arr, item, comparator) {
    arr = angular.isArray(arr) ? arr : [];
      if(!contains(arr, item, comparator)) {
          arr.push(item);
      }
    return arr;
  }  

  // remove
  function remove(arr, item, comparator) {
    if (angular.isArray(arr)) {
      for (var i = arr.length; i--;) {
        if (comparator(arr[i], item)) {
          arr.splice(i, 1);
          break;
        }
      }
    }
    return arr;
  }

  // http://stackoverflow.com/a/19228302/1458162
  function postLinkFn(scope, elem, attrs) {
     // exclude recursion, but still keep the model
    var checklistModel = attrs.checklistModel;
    attrs.$set("checklistModel", null);
    // compile with `ng-model` pointing to `checked`
    $compile(elem)(scope);
    attrs.$set("checklistModel", checklistModel);

    // getter / setter for original model
    var getter = $parse(checklistModel);
    var setter = getter.assign;
    var checklistChange = $parse(attrs.checklistChange);
    var checklistBeforeChange = $parse(attrs.checklistBeforeChange);

    // value added to list
    var value = attrs.checklistValue ? $parse(attrs.checklistValue)(scope.$parent) : attrs.value;


    var comparator = angular.equals;

    if (attrs.hasOwnProperty('checklistComparator')){
      if (attrs.checklistComparator[0] == '.') {
        var comparatorExpression = attrs.checklistComparator.substring(1);
        comparator = function (a, b) {
          return a[comparatorExpression] === b[comparatorExpression];
        };
        
      } else {
        comparator = $parse(attrs.checklistComparator)(scope.$parent);
      }
    }

    // watch UI checked change
    scope.$watch(attrs.ngModel, function(newValue, oldValue) {
      if (newValue === oldValue) { 
        return;
      } 

      if (checklistBeforeChange && (checklistBeforeChange(scope) === false)) {
        scope[attrs.ngModel] = contains(getter(scope.$parent), value, comparator);
        return;
      }

      setValueInChecklistModel(value, newValue);

      if (checklistChange) {
        checklistChange(scope);
      }
    });

    function setValueInChecklistModel(value, checked) {
      var current = getter(scope.$parent);
      if (angular.isFunction(setter)) {
        if (checked === true) {
          setter(scope.$parent, add(current, value, comparator));
        } else {
          setter(scope.$parent, remove(current, value, comparator));
        }
      }
      
    }

    // declare one function to be used for both $watch functions
    function setChecked(newArr, oldArr) {
      if (checklistBeforeChange && (checklistBeforeChange(scope) === false)) {
        setValueInChecklistModel(value, scope[attrs.ngModel]);
        return;
      }
      scope[attrs.ngModel] = contains(newArr, value, comparator);
    }

    // watch original model change
    // use the faster $watchCollection method if it's available
    if (angular.isFunction(scope.$parent.$watchCollection)) {
        scope.$parent.$watchCollection(checklistModel, setChecked);
    } else {
        scope.$parent.$watch(checklistModel, setChecked, true);
    }
  }

  return {
    restrict: 'A',
    priority: 1000,
    terminal: true,
    scope: true,
    compile: function(tElement, tAttrs) {
      if ((tElement[0].tagName !== 'INPUT' || tAttrs.type !== 'checkbox') && (tElement[0].tagName !== 'MD-CHECKBOX') && (!tAttrs.btnCheckbox)) {
        throw 'checklist-model should be applied to `input[type="checkbox"]` or `md-checkbox`.';
      }

      if (!tAttrs.checklistValue && !tAttrs.value) {
        throw 'You should provide `value` or `checklist-value`.';
      }

      // by default ngModel is 'checked', so we set it if not specified
      if (!tAttrs.ngModel) {
        // local scope var storing individual checkbox model
        tAttrs.$set("ngModel", "checked");
      }

      return postLinkFn;
    }
  };
}]);