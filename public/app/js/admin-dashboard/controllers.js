'use strict';
var recruitment = angular.module('RecruitmentControllers', []);

recruitment.controller('RecruitmentCtrl', ['$scope', 'global', '$timeout', 'http', 'logger', function ($scope, global, $timeout, http, logger) {
	$scope.$alerts = [];
	global.alert.listTrigger(function (alert) {console.log(alert);
        $scope.$alerts.push(alert);
        $timeout(function () {
        	$scope.$alerts.splice($scope.$alerts.length - 1, 1);
        }, 3000);
	});

	var pusher = new Pusher('41b2d387872081e26127', {
      encrypted: true
    });

    var channel = pusher.subscribe('applicant_submition_channel');
    channel.bind('refresh_applicant_submition_event', function(data) {
      http.getAllApplicantSubmission({});
      
      http.getAllNotifications({}).then(function (data) {
      	var notify = data.data.$notifications[0];
      	logger.log("<strong>" + notify.applicant.firstname + " " + notify.applicant.lastname + "</strong> has completed <strong>" + notify.questionnaire.name + "</strong> test.");
      });
    });
}]);


recruitment.controller('HeaderCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {
	
	var resetFormData = function () {
		$scope.admin = {
		    firstname: '',
		    lastname: '',
		    email: '',
		    password: ''
		};
		$('.admin-option-settings.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	var resetProperty = function ()
	{
		$scope.notification_property = {
        	new_notification: 0,
        	is_clicked: false
        };
	}

	resetFormData();

	http.getAllNotifications({});

	global.notifications.listTrigger(function (notifications) {
        $scope.$notifications = angular.copy(notifications);
        
        resetProperty();
        
        $scope.$notifications.forEach(function (value, index) {
        	if (value.is_viewed == 0)
        	{
        		$scope.notification_property.new_notification += 1;
        	}
        });
	});

	$scope.onNotificationClicked = function ()
	{
		$scope.notification_property.is_clicked = true;
	}

	$scope.viewNotification = function (notification)
	{
		http.viewNotification({notification_id: notification.id}).then(function (data) {
			location.href = "/admin-dashboard#/grade_results/" + notification.applicants_id;
		});
	}

	// http.getAdminProfileDetails({}).then(function (data) {
	// 	$scope.admin = angular.copy(data.data.$admin);
	// });

	$scope.onUpdateProfile = function () {
		var password = angular.copy($scope.admin.password);
		http.updateAdminProfile($scope.admin).then(function (data) {
			$scope.admin = data.data.$admin;
			$scope.admin.password = password;
			$scope.errors = {};
			$scope.status.isopenSettings = false;
		}, function (data) {
			if (data.errors) {
				$scope.errors = data.errors;
			}
		});
	};

	$scope.onClearForm = function () {
		resetFormData();
	};
}]);


recruitment.controller('User_groupsCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	$scope.$user_groups = [];
    $scope._user_groups = [];
	http.getAllUserGroupsAjax({});
	
	global.user_groups.listTrigger(function (user_groups) {
        $scope.$user_groups = angular.copy(user_groups);
    	$scope._user_groups = angular.copy(user_groups);
	});

	var resetFormData = function () {
		$scope.data = {
		    name: '',
		    description: ''
		};
		$('.user_groups.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();

	$scope.errors = {};

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });

	$scope.hasError = validator.hasError;
	
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.updateForm = false;
	$scope.onSubmitForm = function () {
		if ($scope.updateForm) {
			http.updateUserGroup($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
				$scope.updateForm = false;
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			http.createUserGroup($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		}
	};

	$scope.onUpdate = function (data) {
		$scope.updateForm = true;
		$scope.data = data;
	};

	$scope.onClearForm = function () {
		$scope.errors = {};
		resetFormData();
		if ($scope.updateForm) {
			$scope.updateForm = false;	
		}
	};

	$scope.onDelete = function (data) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteUserGroup(data);
		}
		return false;
	};

	
	$scope.onSearchFilter = function () {
		if ($scope.searchKeywords === '') {
			$scope.$user_groups = angular.copy($scope._user_groups);
			return;
		}
        var user_groups = angular.copy($scope._user_groups);
		var newList = [];
		for (var i in user_groups) {
 			var indexOfValue = user_groups[i].name.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(user_groups[i]);
 			}
		}
		$scope.$user_groups = angular.copy(newList);
	};


	$scope.numPerPageOpt = [3, 5, 10, 20];

	$scope.numPerPage = $scope.numPerPageOpt[2];

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$user_groups =  angular.copy($scope._user_groups.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

}]);

recruitment.controller('User_managementCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	$scope.$users = [];
    $scope._users = [];
    $scope.$user_groups = [];
	http.getAllUserGroupsAjax({});
	global.user_groups.listTrigger(function (user_groups) {
        $scope.$user_groups = angular.copy(user_groups);
	});
	http.getAllUsersAjax({});
	global.users.listTrigger(function (users) {
        $scope.$users = angular.copy(users);
    	$scope._users = angular.copy(users);
	});
	global.users_counter.listTrigger(function (users_counter) {
        $scope.$users_counter = angular.copy(users_counter);
	});


	var resetFormData = function () {
		$scope.data = {
		    user_group_id: '',
		    email: '',
		    firstname: '', 
		    lastname: '',
		    status: 'Active',
		    password: ''
		};
		$('.members.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();
	$scope.errors = {};
	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });
	$scope.hasError = validator.hasError;
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.updateForm = false;
	$scope.onSubmitForm = function () {
		if ($scope.updateForm) {
			http.updateUser($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
				$scope.updateForm = false;
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			http.createUser($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		}
	};

	$scope.onUpdate = function (data) {
		$scope.updateForm = true;
		$scope.data = angular.copy(data);
		$scope.data.status = (data.status === 1) ? 'Active' : 'Inactive';
	};

	$scope.onClearForm = function () {
		$scope.errors = {};
		resetFormData();
		if ($scope.updateForm) {
			$scope.updateForm = false;	
		}
	};

	$scope.onDelete = function (data) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteUser(data);
		}
		return false;
	};

	$scope.onDisabled = function () {

	};
	
	$scope.onSearchFilter = function () {
		if ($scope.searchKeywords === '') {
			$scope.$users = angular.copy($scope._users);
			return;
		}
        var users = angular.copy($scope._users);
		var newList = [];
		for (var i in users) {
 			var indexOfValue = users[i].firstname.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(users[i]);
 			}
		}
		$scope.$users = angular.copy(newList);
	};


	$scope.numPerPageOpt = [3, 5, 10, 20];

	$scope.numPerPage = $scope.numPerPageOpt[2];

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$users =  angular.copy($scope._users.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

    $scope.onGeneratePassword = function () {
		document.getElementById("random-password-field").classList.add('ng-dirty');
		$scope.data.password = Math.random().toString(36).slice(-8);
	};

}]);

recruitment.controller('FieldsCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {
	$scope.type_field = [{
		code: 'textbox',
		text: 'Textbox'
	},{
		code: 'checkbox',
		text: 'Checkbox'
	},{
		code: 'radio-button',
		text: 'Radio Button'
	},{
		code: 'drop-down',
		text: 'Drop Down'
	},{
		code: 'date-picker',
		text: 'Date Picker'
	}];


	$scope.$field_groups = [];
	http.getAllFieldGroupsAjax({});
	global.field_groups.listTrigger(function (field_groups) {
        $scope.$field_groups = angular.copy(field_groups);
	});

	$scope.$fields = [];
	$scope._fields = [];
	http.getAllFieldsAjax({});
	global.fields.listTrigger(function (fields) {
        $scope.$fields = angular.copy(fields);
        $scope._fields = angular.copy(fields);
	});


	$scope.status = {
		0: 'Disabled',
		1: 'Enable'
	};
	var resetFormData = function () {
		$("#choices").html('');

		$scope.data = {
			field_group_id: '',
			model_name: '',
			label: '',
			placeholder: '',
			type_field: '',
			is_required: false,
			status: 'Enable',
			choices: [[{key : 'value',value: ''},{key : 'label', value: ''}]]
		};
		$('.fields.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();
	$scope.errors = {};
	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });
	$scope.hasError = validator.hasError;
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.$watch('data.label', function (value) {
		if (value !== undefined) {
            $scope.data.model_name = value.toLowerCase().replace(new RegExp('[^a-zA-Z0-9\\-\\s]', 'g'), '').replace(/\s+/g, '_');
        } else {
            $scope.data.model_name = '';
        }
	});

	$scope.updateForm = false;

	$scope.onSubmitForm = function () {
		if ($scope.updateForm) {
			http.updateField($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
				$scope.updateForm = false;
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			http.createField($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		}
	};

	$scope.onUpdate = function (data) {
		resetFormData();
		$scope.updateForm = true;
		$scope.data = angular.copy(data);
		$scope.data.status = (data.status === 1) ? 'Enable' : 'Disable';
	};


	$scope.onDelete = function (data) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteField(data);
		}
		return false;
	};

	$scope.addNewRow = function (key) {
		if (key === 'choices') {
			var lastIndex = $scope.data[key].length - 1;
			if (lastIndex === -1) {
				$scope.data[key].push([
					{
						key : 'value',
						value: ''
					},
					{	
						key : 'label',
						value: ''
					}
				]);
			} else if ($scope.data[key][lastIndex][0].value !== '' && 
				$scope.data[key][lastIndex][1].value !== '' || lastIndex === -1) {
				$scope.data[key].push([
					{
						key : 'value',
						value: ''
					},
					{	
						key : 'label',
						value: ''
					}
				]);
			}

		}
	};

	$scope.onRemoveRow = function (data) {
		if ($scope.data[data.key].length > 1) 
			var tempList = angular.copy($scope.data[data.key]);
			var len = tempList.length;
    		$scope.data[data.key] = tempList.slice(0, data.index).concat( tempList.slice(data.index + 1) );	
	};

}]);	

recruitment.controller('Field_assignCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {
	var resetFormData = function () {
		$("#choices").html('');

		$scope.data = {
			field_group_id: '',
			label: '',
			placeholder: '',
			type_field: '',
			is_required: false,
			status: 'Enable',
			choices: [[{key : 'value',value: ''},{key : 'label', value: ''}]]
		};
		$('.fields.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();
	$scope.errors = {};

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });
	$scope.hasError = validator.hasError;
	$scope.showErrorBlock = validator.showErrorBlock;
	
}]);

recruitment.controller('Field_groupsCtrl', ['$scope', 'http', 'validator', 'global', '$timeout', function ($scope, http, validator, global, $timeout) {

	$scope.$field_groups = [];
    $scope._field_groups = [];
	http.getAllFieldGroupsAjax({});
	
	global.field_groups.listTrigger(function (field_groups) {
        $scope.$field_groups = angular.copy(field_groups);
    	$scope._field_groups = angular.copy(field_groups);
	});

	var resetFormData = function () {
		$timeout(function () {
			var iTag = $('.icon-picker-input').find('i')[0];
			if (iTag.getAttribute('class') !== null) {
				iTag.setAttribute('class', '');
			}
		}, 150);
		$scope.data = {
		    name: '',
		    icon: ''
		};
		$('.field_groups.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();

	$scope.errors = {};

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });

	$scope.hasError = validator.hasError;
	
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.updateForm = false;
	$scope.onSubmitForm = function () {console.log($scope.data);
		if ($scope.updateForm) {
			http.updateFieldGroup($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
				$scope.updateForm = false;
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			http.createFieldGroup($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		}
	};
	
	$scope.onUpdateIcon = function (data) {
		$scope.data.icon = data.icon;
	};

	$scope.onUpdate = function (data) {
		$scope.updateForm = true;
		$scope.data = data;

		var iTag = $('.icon-picker-input').find('i')[0];
		iTag.setAttribute('class', data.icon);
	};

	$scope.onClearForm = function () {
		$scope.errors = {};
		resetFormData();
		if ($scope.updateForm) {
			$scope.updateForm = false;	
		}
	};

	$scope.onDelete = function (data) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteFieldGroup(data);
		}
		return false;
	};

	
	$scope.onSearchFilter = function () {
		if ($scope.searchKeywords === '') {
			$scope.$field_groups = angular.copy($scope._field_groups);
			return;
		}
        var field_groups = angular.copy($scope._field_groups);
		var newList = [];
		for (var i in field_groups) {
 			var indexOfValue = field_groups[i].name.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(field_groups[i]);
 			}
		}
		$scope.$field_groups = angular.copy(newList);
	};

	$scope.numPerPageOpt = [3, 5, 10, 20];

	$scope.numPerPage = $scope.numPerPageOpt[2];

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$field_groups =  angular.copy($scope._field_groups.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

}]);


recruitment.controller('Questionaire_groupsCtrl', ['$scope', 'http', 'validator', 'global', '$timeout', function ($scope, http, validator, global, $timeout) {

	$scope.$questionaire_groups = [];
    $scope._questionaire_groups = [];
	http.getAllQuestionaireGroupsAjax({});
	global.questionaire_groups.listTrigger(function (questionaire_groups) {
        $scope.$questionaire_groups = angular.copy(questionaire_groups);
    	$scope._questionaire_groups = angular.copy(questionaire_groups);
	});

	var resetFormData = function () {
		$scope.data = {
		    name: '',
		    description: ''
		};
		$('.questionaire_groups.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();

	$scope.errors = {};

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });

	$scope.hasError = validator.hasError;
	
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.updateForm = false;
	$scope.onSubmitForm = function () {
		if ($scope.updateForm) {
			http.updateQuestionaireGroup($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
				$scope.updateForm = false;
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			http.createQuestionaireGroup($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		}
	};
	
	$scope.onUpdate = function (data) {
		$scope.updateForm = true;
		$scope.data = data;
	};

	$scope.onClearForm = function () {
		$scope.errors = {};
		resetFormData();
		if ($scope.updateForm) {
			$scope.updateForm = false;	
		}
	};

	$scope.onDelete = function (data) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteQuestionaireGroup(data);
		}
		return false;
	};

	
	$scope.onSearchFilter = function () {
		if ($scope.searchKeywords === '') {
			$scope.$questionaire_groups = angular.copy($scope._questionaire_groups);
			return;
		}
        var questionaire_groups = angular.copy($scope._questionaire_groups);
		var newList = [];
		for (var i in questionaire_groups) {
 			var indexOfValue = questionaire_groups[i].name.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(questionaire_groups[i]);
 			}
		}
		$scope.$questionaire_groups = angular.copy(newList);
	};

	$scope.numPerPageOpt = [3, 5, 10, 20];

	$scope.numPerPage = $scope.numPerPageOpt[2];

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$questionaire_groups =  angular.copy($scope._questionaire_groups.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

}]);


recruitment.controller('Question_levelsCtrl', ['$scope', 'http', 'validator', 'global', '$timeout', function ($scope, http, validator, global, $timeout) {

	$scope.$questionaire_groups = [];
	http.getAllQuestionaireGroupsAjax({});
	global.questionaire_groups.listTrigger(function (questionaire_groups) {
        $scope.$questionaire_groups = angular.copy(questionaire_groups);
	});

	$scope.$question_levels = [];
    $scope._question_levels = [];
	http.getAllQuestionLevelsAjax({});
	global.question_levels.listTrigger(function (question_levels) {
        $scope.$question_levels = angular.copy(question_levels);
    	$scope._question_levels = angular.copy(question_levels);
	});

	var resetFormData = function () {
		$scope.data = {
		    name: '',
		    description: '',
		    questionaire_group_id: ''
		};
		$('.question_levels.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();

	$scope.errors = {};

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });

	$scope.hasError = validator.hasError;
	
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.updateForm = false;
	$scope.onSubmitForm = function () {
		if ($scope.updateForm) {
			http.updateQuestionLevel($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
				$scope.updateForm = false;
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			http.createQuestionLevel($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		}
	};
	
	$scope.onUpdate = function (data) {
		$scope.updateForm = true;
		$scope.data = data;
	};

	$scope.onClearForm = function () {
		$scope.errors = {};
		resetFormData();
		if ($scope.updateForm) {
			$scope.updateForm = false;	
		}
	};

	$scope.onDelete = function (data) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteQuestionLevel(data);
		}
		return false;
	};

	
	$scope.onSearchFilter = function () {
		if ($scope.searchKeywords === '') {
			$scope.$question_levels = angular.copy($scope._question_levels);
			return;
		}
        var question_levels = angular.copy($scope._question_levels);
		var newList = [];
		for (var i in question_levels) {
 			var indexOfValue = question_levels[i].name.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(question_levels[i]);
 			}
		}
		$scope.$question_levels = angular.copy(newList);
	};

	$scope.numPerPageOpt = [3, 5, 10, 20];

	$scope.numPerPage = $scope.numPerPageOpt[2];

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$question_levels =  angular.copy($scope._question_levels.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

}]);


recruitment.controller('ArticlesCtrl', ['$scope', 'http', 'validator', 'global', '$timeout', function ($scope, http, validator, global, $timeout) {

	$scope.$question_levels = [];
	http.getAllQuestionLevelsAjax({});
	global.question_levels.listTrigger(function (question_levels) {
        $scope.$question_levels = angular.copy(question_levels);
	});

	$scope.$articles = [];
    $scope._articles = [];
	http.getAllArticlesAjax({});
	global.articles.listTrigger(function (articles) {
        $scope.$articles = angular.copy(articles);
    	$scope._articles = angular.copy(articles);
	});

	var resetFormData = function () {
		$scope.data = {
		    name: '',
		    description: '',
		    question_level_id: '',
		    video_url: ''
		};
		$('.articles.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();

	$scope.errors = {};

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });

	$scope.hasError = validator.hasError;
	
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.updateForm = false;
	$scope.onSubmitForm = function () {
		if ($scope.updateForm) {
			http.updateArticle($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
				$scope.updateForm = false;
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			http.createArticle($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		}
	};
	
	$scope.onUpdate = function (data) {
		$scope.updateForm = true;
		$scope.data = data;
	};

	$scope.onClearForm = function () {
		$scope.errors = {};
		resetFormData();
		if ($scope.updateForm) {
			$scope.updateForm = false;	
		}
	};

	$scope.onDelete = function (data) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteArticle(data);
		}
		return false;
	};

	
	$scope.onSearchFilter = function () {
		if ($scope.searchKeywords === '') {
			$scope.$articles = angular.copy($scope._articles);
			return;
		}
        var articles = angular.copy($scope._articles);
		var newList = [];
		for (var i in articles) {
 			var indexOfValue = articles[i].name.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(articles[i]);
 			}
		}
		$scope.$articles = angular.copy(newList);
	};

	$scope.numPerPageOpt = [3, 5, 10, 20];

	$scope.numPerPage = $scope.numPerPageOpt[2];

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$articles =  angular.copy($scope._articles.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

}]);



recruitment.controller('QuestionsCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {
	$scope.type_field = [{
		code: 'textbox',
		text: 'Textbox'
	},{
		code: 'radio-button',
		text: 'Radio Button'
	}];

	$scope.$questionaire_groups = [];
	http.getAllQuestionaireGroupsAjax({});
	global.questionaire_groups.listTrigger(function (questionaire_groups) {
        $scope.$questionaire_groups = angular.copy(questionaire_groups);
	});

	$scope.$watch('data.questionaire_group', function (value) {
		$scope.$question_levels = [];
		$scope.$articles = [];
		$scope.data.question_level = '';
		$scope.data.article = '';
		if (value !== '') { 
        	http.getAllQuestionLevelsAjax({'questionaire_group_id': value});
        }
	});

	$scope.$watch('data.question_level', function (value) {
		$scope.$articles = [];
		$scope.data.article = '';
		if (value !== '') { 
        	http.getAllArticlesAjax({'question_level_id': value});
        }
	});
	$scope.$question_levels = [];
	global.question_levels.listTrigger(function (question_levels) {
        $scope.$question_levels = angular.copy(question_levels);
	});

	$scope.$articles = [];
	global.articles.listTrigger(function (articles) {
        $scope.$articles = angular.copy(articles);
	});

	$scope.$questions = [];
	$scope._questions = [];
	http.getAllQuestionsAjax({});
	global.questions.listTrigger(function (questions) {
        $scope.$questions = angular.copy(questions);
        $scope._questions = angular.copy(questions);
	});


	$scope.status = {
		0: 'Disabled',
		1: 'Enable'
	};
	var resetFormData = function () {
		$("#choices").html('');

		$scope.data = {
			questionaire_group: '',
			question_level: '',
			article: '',
			model_name: '',
			name: '',
			value_answer: '',
			type_field: '',
			status: 'Enable',
			choices: [[{key : 'value',value: ''},{key : 'label', value: ''}]]
		};
		$('.questions.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();
	$scope.errors = {};
	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });
	$scope.hasError = validator.hasError;
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.$watch('data.name', function (value) {
		if (value !== undefined) {
            $scope.data.model_name = value.toLowerCase().replace(new RegExp('[^a-zA-Z0-9\\-\\s]', 'g'), '').replace(/\s+/g, '_');
        } else {
            $scope.data.model_name = '';
        }
	});

	$scope.updateForm = false;

	$scope.onSubmitForm = function () {
		if ($scope.updateForm) {
			http.updateQuestion($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
				$scope.updateForm = false;
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			http.createQuestion($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		}
	};

	$scope.onClearForm = function () {
		$scope.errors = {};
		resetFormData();
		if ($scope.updateForm) {
			$scope.updateForm = false;	
		}
	};


	$scope.onUpdate = function (data) {
		resetFormData();
		$scope.updateForm = true;
		data.question_level = data.question_level_id;
		data.article = (data.article_id == -1) ? '' : data.article_id;
		$scope.data = angular.copy(data);
		$scope.data.status = (data.status === 1) ? 'Enable' : 'Disable';
	};


	$scope.onDelete = function (data) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteQuestion(data);
		}
		return false;
	};

	$scope.addNewRow = function (key) {
		if (key === 'choices') {
			var lastIndex = $scope.data[key].length - 1;
			if (lastIndex === -1) {
				$scope.data[key].push([
					{
						key : 'value',
						value: ''
					},
					{	
						key : 'label',
						value: ''
					}
				]);
			} else if ($scope.data[key][lastIndex][0].value !== '' && 
				$scope.data[key][lastIndex][1].value !== '' || lastIndex === -1) {
				$scope.data[key].push([
					{
						key : 'value',
						value: ''
					},
					{	
						key : 'label',
						value: ''
					}
				]);
			}

		}
	};

	$scope.onRemoveRow = function (data) {
		if ($scope.data[data.key].length > 1) 
			var tempList = angular.copy($scope.data[data.key]);
			var len = tempList.length;
    		$scope.data[data.key] = tempList.slice(0, data.index).concat( tempList.slice(data.index + 1) );	
	};

}]);

recruitment.controller('Questionnaire_typesCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	$scope.errors = {};
	$scope.$questionnaires = [];

	var resetForm = function () {
		$("#criteria").html('');

		$scope.data = {
			name: '',
			color: 'default',
			criteria: [[{key : 'criteria',value: ''},{key : 'passing', value: ''}]],
			is_active: 0
		};
	}

	resetForm();

	http.getQuestionnaires({}).then(function (data) {
		$scope.errors = {};
		$scope.$questionnaires = data.data.$questionnaires;
	}, function (data) {
		if (data.errors) {
			$scope.errors = data.errors;
		}
	});

	$scope.addNewRow = function (key) {
		if (key === 'criteria') {
			var lastIndex = $scope.data[key].length - 1;
			if (lastIndex === -1) {
				$scope.data[key].push([
					{
						key : 'criteria',
						value: ''
					},
					{	
						key : 'passing',
						value: ''
					}
				]);
			} else if ($scope.data[key][lastIndex][0].value !== '' && 
				$scope.data[key][lastIndex][1].value !== '' || lastIndex === -1) {
				$scope.data[key].push([
					{
						key : 'criteria',
						value: ''
					},
					{	
						key : 'passing',
						value: ''
					}
				]);
			}

		}
	};

	$scope.onRemoveRow = function (data) {
		if ($scope.data[data.key].length > 1) 
			var tempList = angular.copy($scope.data[data.key]);
			var len = tempList.length;
    		$scope.data[data.key] = tempList.slice(0, data.index).concat( tempList.slice(data.index + 1) );	
	};

	$scope.onSubmitForm = function () {
		$scope.data.criteria = JSON.stringify($scope.data.criteria);

		http.createNewQuestionnaire($scope.data).then(function (data) {
			$scope.errors = {};
			$scope.$questionnaires = data.data.$questionnaires;
			resetForm();
		}, function (data) {
			if (data.errors) {
				$scope.errors = data.errors;
			}
		});
	}
}]);

recruitment.controller('Questionnaire_pagesCtrl', ['$scope', 'http', 'validator', 'global', '$sce', function ($scope, http, validator, global, $sce) {
	$scope.$questionnaires = false;
	$scope.old_page_items = {};

	$scope.choices_letter = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

	$scope.is_changed_button_enable = false;
	
	$scope.show_tool_menu = true;
	$scope.edit_field_mode = false;
	$scope.old_field_attributes = [];
	
	$scope.field_position = 1;

	$scope.errors = {};

	$scope.pages = {};

	var resetSelection = function ()
	{
		$scope.selected = {questionnaire: {}, page: {}, page_id: 0, total_questionnaire_page: 0};
		$scope.pages = {};
	}

	var doResetTool = function ()
	{
		$scope.tools = {
			header_text_tool_visible: false,
			text_tool_visible: false,
			media_tool_visible: false,
			question_tool_visible: false
		};

		$scope.show_tool_menu = true;
	};
	
	var objectNullValueRemover = function (object)
	{
		var object_holder = [];

		if (typeof object.forEach === 'function') {
			object.forEach(function (value, index) {
				if (!jQuery.isEmptyObject(value))
				{
					object_holder[index] = value;
				}
			});
		} else {
			for (var i in object)
			{
				if (!jQuery.isEmptyObject(object[i]))
				{
					object_holder[i] = object[i];
				}
			}
		}

		return object_holder;
	}

	$scope.objectReturnIndexOf = function (object, element)
	{
		for (var i in object)
		{
			if (object[i] == element)
				return i;
		}

		return false;
	}

	$scope.trustResourceURL = function (url)
	{
		return $sce.trustAsResourceUrl(url);
	}

	$scope.fieldMouseOver = function (option_id)
	{
		$("#" + option_id).show();
	}

	$scope.fieldMouseLeave = function (option_id)
	{
		$("#" + option_id).hide();
	}

	$scope.inArray = function (search, source)
	{
		for (var i = 0; i < source.length; i++)
		{
			if (source[i] == search)
				return true;
		}

		return false;
	}

	var sortArray = function (array)
	{
		return array.sort(function(a, b) {
			return a - b;
		});
	}

	var calculateLastFieldPosition = function ()
	{
		var page_items = $scope.selected.page.page_items;
		if (page_items[Object.keys(page_items)[Object.keys(page_items).length - 1]] !== undefined) {
			$scope.field_position = page_items[Object.keys(page_items)[Object.keys(page_items).length - 1]].position + 1;
		} else {
			$scope.field_position = 1;
		}
	}

	var calculateQuestionnairePages = function ()
	{
		var page_counter = 1;
		$scope.pages = {};
	
		for (var prop in $scope.selected.questionnaire.pages)
		{	
			if (typeof $scope.selected.questionnaire.pages[prop] === 'object')
			{
				$scope.pages[page_counter] = prop;
				page_counter += 1;
			}
		}
		$scope.selected.total_questionnaire_page = page_counter - 1;
		$scope.old_page_items = angular.copy($scope.selected.page.page_items);
	}

	var is_changed = function ()
	{
		var current = angular.toJson(objectNullValueRemover($scope.selected.page.page_items));
		var old = angular.toJson(objectNullValueRemover($scope.old_page_items));
		return JSON.stringify(current) !== JSON.stringify(old);
	}


	resetSelection();


	/*+-----------------------------------+*/
	/*|   STARTING POINT OF THE EDITOR    |*/
	/*+-----------------------------------+*/

	var load = function ()
	{
		doResetTool();

		http.getQuestionnaires({}).then(function (data) {
			$scope.errors = {};
			$scope.$questionnaires = data.data.$questionnaires;

			/* Convert questionnaire page_item string to JSON */
			for (var prop_1 in $scope.$questionnaires)
			{
				for (var prop_2 in $scope.$questionnaires[prop_1].pages)
				{
					if (typeof $scope.$questionnaires[prop_1].pages[prop_2] === 'object')
					{
						/* Convert string to JSON */
						$scope.$questionnaires[prop_1].pages[prop_2].page_items = JSON.parse($scope.$questionnaires[prop_1].pages[prop_2].page_items);
						/* Convert string to JSON */
						if (typeof $scope.$questionnaires[prop_1].criteria === 'string')
							$scope.$questionnaires[prop_1].criteria = JSON.parse($scope.$questionnaires[prop_1].criteria);
						/* Remove object null values */
						$scope.$questionnaires[prop_1].pages[prop_2].page_items = objectNullValueRemover($scope.$questionnaires[prop_1].pages[prop_2].page_items);
					}
				}
			}


			if ($scope.selected.questionnaire.hasOwnProperty('id') || $scope.selected.page.hasOwnProperty('id'))
			{
				$scope.selected.questionnaire = $scope.$questionnaires[$scope.selected.questionnaire.id];
				calculateQuestionnairePages();

				$scope.selected.page = $scope.$questionnaires[$scope.selected.questionnaire.id].pages[$scope.selected.page.id];
				$scope.selected.page_id = $scope.selected.page.id;

				$scope.old_page_items = angular.copy($scope.selected.page.page_items);

				$scope.is_changed_button_enable = is_changed();
			}





			/*+-----------------------------------+*/
			/*| SELECTING QUESTIONNAIRE AND PAGES |*/
			/*+-----------------------------------+*/

			$scope.doSelectQuestionnaire = function ()
			{
				if ($scope.selected.questionnaire === null)
				{
					resetSelection();
					return;
				}

				calculateQuestionnairePages();
			}

			$scope.doSelectQuestionnairePage = function ()
			{
				if ($scope.selected.page_id === null)
				{
					resetSelection();
					return;
				}

				$scope.selected.page = $scope.selected.questionnaire.pages[$scope.selected.page_id];

				$scope.old_page_items = angular.copy($scope.selected.page.page_items);
			}

			$scope.newPage = function ()
			{
				var data = {
					questionnaire_id: $scope.selected.questionnaire.id,
				};

				http.createNewPage(data).then(function (data) {
					$scope.errors = {};
					$scope.selected.questionnaire = data.data.$questionnaire[$scope.selected.questionnaire.id];
					$scope.selected.page = $scope.selected.questionnaire.pages[data.data.$newPage.id];
					$scope.selected.page_id = data.data.$newPage.id;

					load();

					/*doGetQuestionnaires(function() {
						$scope.$selected_questionnaire = $scope.$questionnaires[$scope.data.questionnaire_id];
						$scope.$selected_page = $scope.$questionnaires[$scope.data.questionnaire_id]['pages'][data.data.$page.id];
						
						if (typeof $scope.$selected_page.page_items === 'string')
						{
							$scope.data.page_items = JSON.parse($scope.$selected_page.page_items);
						}
						
						$scope.old_page_items = angular.copy($scope.data.page_items);
						$scope.data.page_sequence = $scope.$selected_page.page_sequence;
						$scope.data.page_id = $scope.$selected_page.id;
						initializeFieldPosition();
					});*/
					
				}, function (data) {
					if (data.errors) {
						$scope.errors = data.errors;
					}
				});
			}

			$scope.deletePage = function ()
			{
				if (window.confirm('Are you sure you want to delete this page?'))
				{
					var data = {
						page_id: $scope.selected.page_id
					};

					http.deletePage(data).then(function (data) {
						$scope.errors = {};

						resetSelection();
						load();
					}, function (data) {
						if (data.errors) {
							$scope.errors = data.errors;
						}
					});
				}
			}

			

			


			/*+-----------------------------------+*/
			/*| ADD/REMOVE AND CANCEL TOOL EVENTS |*/
			/*+-----------------------------------+*/

			$scope.saveFieldAttributes = function ()
			{
				$scope.field_position = $scope.field_position + 1;

				$scope.is_changed_button_enable = is_changed();
				
				doResetTool();
			}

			$scope.removePageField = function (field_position)
			{
				$scope.selected.page.page_items[field_position] = {};
				$scope.selected.page.page_items = objectNullValueRemover($scope.selected.page.page_items);

				$scope.is_changed_button_enable = is_changed();
			}

			$scope.cancelTool = function ()
			{
				if (!$scope.edit_field_mode) {
					$scope.selected.page.page_items[$scope.field_position] = {};
				} else {
					$scope.selected.page.page_items[$scope.field_position] = $scope.old_field_attributes;
				}
				
				$scope.is_changed_button_enable = is_changed();
				doResetTool();
			}





			/*+--------------------+*/
			/*| TOOL ACTION EVENTS |*/
			/*+--------------------+*/

			$scope.addEditQuestonnaireField = function (object)
			{
				if (object.action_type == 'add') {
					calculateLastFieldPosition();

					$scope.selected.page.page_items[$scope.field_position] = {
						field_type: object.data.type,
						questionnaire_id: $scope.selected.questionnaire.id,
						page_id: $scope.selected.page_id,
						position: $scope.field_position,
						criteria: $scope.selected.questionnaire.criteria[0][0].value,
						attributes: {
							value: (object.data.type == 'media') ? '' : object.data.type,
							size: (object.data.type == 'heading') ? '1' : '12px',
							alignment: 'left',
							emphasis: {
								underline: false,
								bold: false,
								italic: false
							},
							choices: {
								choice_type: 'SINGLE SELECTION',
								choices_attributes: []
							},
							answers: []
						}
					};
				} else {
					$scope.edit_field_mode = true;
					$scope.field_position = object.data.position;
					$scope.old_field_attributes = angular.copy($scope.selected.page.page_items[$scope.field_position]);
				}

				switch (object.data.type) {
					case 'heading':
						$scope.tools.header_text_tool_visible = true;
						break;
					case 'text':
						$scope.tools.text_tool_visible = true;
						break;
					case 'media':
						$scope.tools.media_tool_visible = true;
						break;
					case 'question':
						$scope.tools.question_tool_visible = true;
						break;
					default:
						$scope.tools.header_text_tool_visible = true;
				}

				$scope.show_tool_menu = false;
			}

			$scope.addQuestionCriteria = function (criteria)
			{
				$scope.selected.page.page_items[$scope.field_position].criteria = criteria;
			}

			$scope.addFieldSize = function (size)
			{
				$scope.selected.page.page_items[$scope.field_position].attributes.size = size;
			}

			$scope.addFieldAlignment = function (alignment)
			{
				$scope.selected.page.page_items[$scope.field_position].attributes.alignment = alignment;
			}

			$scope.addFieldEmphasis = function (emphasis)
			{
				if ($scope.selected.page.page_items[$scope.field_position].attributes.emphasis[emphasis]) {
					$scope.selected.page.page_items[$scope.field_position].attributes.emphasis[emphasis] = false;
				} else {
					$scope.selected.page.page_items[$scope.field_position].attributes.emphasis[emphasis] = true;
				}
			}

			$scope.setQuestionAnswer = function (field_position)
			{
				if ($scope.selected.page.page_items[field_position].attributes.answers instanceof Array)
				{
					$scope.selected.page.page_items[field_position].attributes.answers = sortArray($scope.selected.page.page_items[field_position].attributes.answers);
				}

				$scope.is_changed_button_enable = is_changed();
			}

			$scope.addChoicesType = function (type)
			{
				$scope.selected.page.page_items[$scope.field_position].attributes.choices.choice_type = type;

				var field = $scope.selected.page.page_items[$scope.field_position];
				var choices = $scope.selected.page.page_items[$scope.field_position].attributes.choices.choices_attributes;

				choices.forEach(function(value, index) {
					value.choice_name = (type == 'SINGLE SELECTION') ? field.field_type + $scope.field_position : field.field_type + $scope.field_position + '[]'
				});
			}

			$scope.addChoice = function ()
			{
				var field = $scope.selected.page.page_items[$scope.field_position];
				var choices = $scope.selected.page.page_items[$scope.field_position].attributes.choices.choices_attributes;

				choices.push({
					choice_letter: $scope.choices_letter[choices.length],
					choice_name: (field.attributes.choices.choice_type == "SINGLE SELECTION") ? field.field_type + $scope.field_position : field.field_type + $scope.field_position + '[]',
					choice_text: 'Choice letter ' + $scope.choices_letter[choices.length]
				});

				$scope.selected.page.page_items[$scope.field_position].attributes.choices.choices_attributes = choices;
			}





			/*+----------------------+*/
			/*| SAVING CHANGES EVENT |*/
			/*+----------------------+*/

			$scope.savePageChanges = function ()
			{
				var data = {
					page_id: $scope.selected.page_id,
					page_items: JSON.stringify($scope.selected.page.page_items)
				};

				http.savePageChanges(data).then(function (data) {
					$scope.errors = {};

					load();
				}, function (data) {
					if (data.errors) {
						$scope.errors = data.errors;
					}
				});
			}
		}, function (data) {
			if (data.errors) {
				$scope.errors = data.errors;
			}
		});
	};

	load();
}]);

recruitment.controller('QuestionnairePagesController', ['$scope', 'http', 'validator', 'global', '$sce', '$routeParams', function ($scope, http, validator, global, $sce, $routeParams) {
	$scope.data = {
		questionnaire_id: $routeParams.questionnaire_id,
		page: 1
	};

	$scope.pages = {};

	var cleanArrayObject = function (object)
	{
		var object_holder = [];

		object.forEach(function (value, index) {
			if (!jQuery.isEmptyObject(value))
			{
				object_holder[index] = value;
			}
		});

		return object_holder;
	}

	http.getQuestionnaire($scope.data).then(function (data) {
		$scope.$selected_questionnaire = data.data.$questionnaire[$scope.data.questionnaire_id];

		var page_counter = 1;
		for (var index in $scope.$selected_questionnaire.pages)
		{
			if (index !== 'length')
			{
				$scope.pages[page_counter] = $scope.$selected_questionnaire.pages[index];
				$scope.pages[page_counter].page_items = cleanArrayObject(JSON.parse($scope.$selected_questionnaire.pages[index].page_items));
				page_counter += 1;
			}
		}

		$scope.trustResourceURL = function (url)
		{
			return $sce.trustAsResourceUrl(url);
		}

		$scope.turnPageTo = function (page)
		{
			$scope.data.page = page;
		}
	});
}]);


recruitment.controller('GradesCtrl', ['$scope', 'http', 'validator', 'global', '$location', function ($scope, http, validator, global, $location) {
	
	$scope.$applicant_submission = [];
	http.getAllApplicantSubmission({});
	$scope.passed = [];
    $scope.failed = [];
	global.applicant_submission.listTrigger(function (applicant_submission) {
        $scope.$applicant_submission = angular.copy(applicant_submission);
        var passed = [];
        var failed = [];
        for(var i in applicant_submission) {
        	var list_criteria = applicant_submission[i].result.list_criteria;
			var passing_criteria = JSON.parse(applicant_submission[i].result['passing_criteria']);
			var criterias = [];
			var testFail = [];
			for(var j in passing_criteria) {
				var criteria = passing_criteria[j][0].value.toLowerCase();
				testFail.push(parseInt(applicant_submission[i].result[criteria]) >= parseInt(passing_criteria[j][1].value));
			}
			var isPassed = true;
			for(var k in testFail) {
				if (!testFail[k]) {
					isPassed = false;
					break;
				}
			}
			if (isPassed) {
				passed.push(applicant_submission[i]);
			} else {
				failed.push(applicant_submission[i]);
			}
        }
        $scope.passed = angular.copy(passed);
        $scope.failed = angular.copy(failed);
	});

	$scope.viewGradeResults = function () {
		$location.path('/grade_results')
	};

	$scope.goToLocation = function (path) {
		$location.path(path);
	};

	$scope.isPassedOrFailed = function (data) {
		var list_criteria = data.result.list_criteria;
		var passing_criteria = JSON.parse(data.result['passing_criteria']);
		var criterias = [];
		var testFail = [];
		for(var i in passing_criteria) {
			var criteria = passing_criteria[i][0].value.toLowerCase();
			criterias.push({
				key: criteria,
				value: parseInt(data.result[criteria]),
				passing: parseInt(passing_criteria[i][1].value),
				total: data.result[criteria + '_total_number_question']
			});
			testFail.push(parseInt(data.result[criteria]) >= parseInt(passing_criteria[i][1].value));
		}
		for(var i in testFail) {
			if (!testFail[i]) {
				return 'down';
			}
		}
		return 'up';
	};
}]);

recruitment.controller('Grade_resultsCtrl', ['$scope', 'http', 'validator', 'global', '$routeParams', '$location', function ($scope, http, validator, global, $routeParams, $location) {
	$scope.onBackView = function () {
    	$location.path('/grades');
    };

    $scope.onViewTestAnswers = function () {
    	$location.path('/results_comparison/' + $routeParams.applicant_id); 
    };
    $scope.criterias = [];
    $scope.applicant = {};
    http.getGradeResultsByApplicantId({applicant_id : $routeParams.applicant_id}).then(function (data) {
    	var data = data.data.$applicant_results;
    	var list_criteria = data.result.list_criteria;
		var passing_criteria = JSON.parse(data.result['passing_criteria']);
		var criterias = [];
		for(var i in passing_criteria) {
			var criteria = passing_criteria[i][0].value.toLowerCase();
			criterias.push({
				key: criteria,
				value: parseInt(data.result[criteria]),
				passing: parseInt(passing_criteria[i][1].value),
				total: data.result[criteria + '_total_number_question']
			});
		}
		$scope.criterias = angular.copy(criterias);
		$scope.applicant = angular.copy(data.applicants[0]);
		$scope.applicant.questionnaire_name = data.questionnaire_name;
    });
    $scope.toUpperCaseFirstLetter = function (str) {
    	return str.charAt(0).toUpperCase() + str.slice(1);
    };

    $scope.passOrFail = function (value, passing) {
    	return (parseInt(value) >= parseInt(passing)) ? 'success' : 'danger';
    };
}]);

recruitment.controller('Results_comparisonCtrl', ['$scope', 'http', 'validator', 'global', '$routeParams', '$location', function ($scope, http, validator, global, $routeParams, $location) {

	http.getAllQuestionnairesByApplicantId({applicant_id : $routeParams.applicant_id});
    global.questionnaire_page.listTrigger(function (questionnaire_page) {console.log(questionnaire_page);
        if (questionnaire_page.length) {
            $scope.$questionnaire_page = angular.copy(questionnaire_page);
        }
    });

    $scope.page_number = 0;
    $scope.turnPageTo = function (page_number) {console.log($scope.$questionnaire_page);
        if (page_number >= 0 && page_number < $scope.$questionnaire_page.length)
            $scope.page_number = page_number;
    };

    $scope.onBackView = function () {
    	$location.path('/grades');
    };

    $scope.checkCorrectAnswer = function (key, answer) {
        return (answer.indexOf(String(key)) !== -1) ? 'bg-success' : '';
    };

    $scope.toStringKey = function (key) {
    	return String(key);
    };


    $scope.onViewTestResults = function () {
    	$location.path('/grade_results/' + $routeParams.applicant_id); 
    };

}]);



