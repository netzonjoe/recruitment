<!doctype html>
<!--[if lt IE 8]>         <html class="no-js lt-ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Web Application</title>
        <meta name="description" content="Responsive Admin Web App with Bootstrap and AngularJS">
        <meta name="keywords" content="angularjs admin, admin templates, admin themes, bootstrap admin">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

        <!-- Needs images, font... therefore can not be part of main.css -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,600italic,400,600,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{ asset('/magic/client/fonts/themify-icons/themify-icons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/font-awesome/css/font-awesome.min.css') }}">
        <!-- end Needs images -->
            
            <!-- build:css({.tmp,client}) styles/main.css -->
            <link rel="stylesheet" href="{{ asset('/magic/client/styles/bootstrap.css') }}">
            <link rel="stylesheet" href="{{ asset('/magic/client/styles/ui.css') }}">
            <link rel="stylesheet" href="{{ asset('/magic/client/styles/main.css') }}">
            <link rel="stylesheet" href="{{ asset('/magic/client/styles/styles.css') }}">
            <!-- endbuild -->
        


    </head>
    <body class="app ng-scope body-wide body-auth">

    <div class="main-container">
        <div id="content" class="content-container">
            <section id="animaite-section"class="view-container animate-fade-up ng-animate ng-enter ng-enter-active" >
             @yield('content')
            </section>  
        </div>
    </div>



        <script src="http://maps.google.com/maps/api/js"></script>
        <!-- build:js scripts/vendor.js -->
        <script src="{{ asset('/magic/client/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/angular/angular.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/angular-route/angular-route.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/angular-aria/angular-aria.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/angular-animate/angular-animate.min.js') }}"></script>
        <!-- endbuild -->

        <!-- build:js scripts/ui.js -->
        <script src="{{ asset('/magic/client/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/jquery.slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/angular-loading-bar/build/loading-bar.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/angular-scroll/angular-scroll.min.js') }}"></script>

        <script src="{{ asset('/magic/client/bower_components/ngmap/build/scripts/ng-map.min.js') }}"></script>

        <script src="{{ asset('/magic/client/bower_components/textAngular/dist/textAngular-rangy.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/textAngular/dist/textAngular-sanitize.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/textAngular/dist/textAngular.min.js') }}"></script>

        <script src="{{ asset('/magic/client/bower_components/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/fullcalendar/dist/fullcalendar.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/angular-ui-calendar/src/calendar.js') }}"></script>

        <script src="{{ asset('/magic/client/bower_components/angular-translate/angular-translate.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js') }}"></script>

        <script src="{{ asset('/magic/client/bower_components/ng-tags-input/ng-tags-input.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/angular-ui-tree/dist/angular-ui-tree.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/angular-wizard/dist/angular-wizard.min.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/bootstrap-file-input/bootstrap.file-input.js') }}"></script>
        <script src="{{ asset('/magic/client/bower_components/angular-validation-match/dist/angular-validation-match.min.js') }}"></script>

        <script src="{{ asset('/magic/client/vendors/echarts.js') }}"></script>         
        <script src="{{ asset('/magic/client/vendors/ngecharts.js') }}"></script>
        <!-- endbuild -->


        @yield('scripts-files')
        
        <!-- build:js({.tmp,client}) scripts/app.js -->
        <!-- injector:js -->
        <script src="{{ asset('/magic/client/app/app.module.js') }}"></script>
        <script src="{{ asset('/magic/client/app/app/calendar/calendar.module.js') }}"></script>
        <script src="{{ asset('/magic/client/app/app/task/task.module.js') }}"></script>
        <script src="{{ asset('/magic/client/app/chart/chart.module.js') }}"></script>
        <script src="{{ asset('/magic/client/app/form/form.module.js') }}"></script>
        <script src="{{ asset('/magic/client/app/form/formValidation.module.js') }}"></script>
        <script src="{{ asset('/magic/client/app/layout/nav.module.js') }}"></script>
        <script src="{{ asset('/magic/client/app/page/page.module.js') }}"></script>
        <script src="{{ asset('/magic/client/app/table/table.module.js') }}"></script>
        <script src="{{ asset('/magic/client/app/ui/ui.module.js') }}"></script>
        <script src="{{ asset('/magic/client/app/app/calendar/calendar.controller.js') }}"></script>
        <script src="{{ asset('/magic/client/app/app/task/task.controller.js') }}"></script>
        <script src="{{ asset('/magic/client/app/app/task/task.directive.js') }}"></script>
        <script src="{{ asset('/magic/client/app/app/task/task.service.js') }}"></script>
        <script src="{{ asset('/magic/client/app/chart/echarts.controller.js') }}"></script>
        <script src="{{ asset('/magic/client/app/core/app.config.js') }}"></script>
        <script src="{{ asset('/magic/client/app/core/app.controller.js') }}"></script>
        <!-- <script src="{{ asset('/magic/client/app/core/config.route.js') }}"></script> -->
        <script src="{{ asset('/app/js/admin-dashboard/lang/i18n.js') }}"></script>
        <script src="{{ asset('/magic/client/app/dashboard/dashboard.controller.js') }}"></script>
        <script src="{{ asset('/magic/client/app/form/form.controller.js') }}"></script>
        <script src="{{ asset('/magic/client/app/form/form.directive.js') }}"></script>
        <script src="{{ asset('/magic/client/app/form/formValidation.controller.js') }}"></script>
        <script src="{{ asset('/magic/client/app/form/wizard.controller.js') }}"></script>
        <script src="{{ asset('/magic/client/app/layout/nav.directive.js') }}"></script>
        <script src="{{ asset('/magic/client/app/page/page.controller.js') }}"></script>
        <script src="{{ asset('/magic/client/app/page/page.directive.js') }}"></script>
        <script src="{{ asset('/magic/client/app/table/table.controller.js') }}"></script>
        <script src="{{ asset('/magic/client/app/ui/ui.controller.js') }}"></script>
        <script src="{{ asset('/magic/client/app/ui/ui.directive.js') }}"></script>
        <script src="{{ asset('/magic/client/app/ui/ui.service.js') }}"></script>

        
        <script src="{{ asset('/app/js/admin-dashboard/routes.js') }}"></script>
        <script src="{{ asset('/app/js/admin-dashboard/services.js') }}"></script>
        <script src="{{ asset('/app/js/admin-dashboard/directives.js') }}"></script>
        <script src="{{ asset('/app/js/admin-dashboard/controllers.js') }}"></script>

        <!-- endinjector -->
        <!-- endbuild -->
    </body>
</html>
