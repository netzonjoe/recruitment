@extends('layouts.guest')


@section('content')



<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-0">

            <div class="panel panel-default">
                <div class="panel-heading">Account Information</div>
                <div class="panel-body">
                    <form class="form-horizontal" id="submit-form-registration" role="form" method="POST" 
                          action="{{ action('GuestUserController@submitvacancy') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}">

                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">

                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control input-medium bfh-phone " name="phone" value="{{ old('phone') }}" data-format="(ddd) ddd-dddd">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Register Now
                                </button>
                            </div>
                        </div> -->
                    </form>
                </div>
            </div>
            
        </div>



        <div class="col-md-4" id="submitvacancyDIV_1" >
            <div id="submitvacancyDIV_2">
                <h2 id="submitvacancyH2_3">
                    <span id="submitvacancySPAN_4">Submit Vacancy</span><br id="submitvacancyBR_5" />
                </h2>
                <p id="submitvacancyP_6">
                    <span id="submitvacancySPAN_7">To Submit a job vacancy you will need to create an account first, please enter your details then click “Register Now” below.</span>
                </p>
            </div>
            <div id="submitvacancyDIV_8">
                <div id="submitvacancyDIV_9">
                </div>
                <div id="submitvacancyDIV_10">
                    <p id="submitvacancyP_11">
                        <a onclick="$('#submit-form-registration')[0].submit();" href="javascript: mmjs.checkout(true);" id="submitvacancyA_12">Register Now</a>
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>



@endsection