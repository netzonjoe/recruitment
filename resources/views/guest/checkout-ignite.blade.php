@extends('layouts.guest')


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-0">

            <form class="form-horizontal" id="submit-form-registration" role="form" method="POST" 
                          action="{{ action('GuestUserController@postCheckoutIgnite') }}">

                <div class="panel panel-default">
                    <div class="panel-heading">Account Information</div>
                    <div class="panel-body">
                        
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}">

                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">

                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control input-medium bfh-phone " name="phone" value="{{ old('phone') }}" data-format="(ddd) ddd-dddd">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
                

                 <div class="panel panel-default">
                    <div class="panel-heading">Billing Details</div>
                    <div class="panel-body">

                        <div class="form-group">
                            <label class="col-md-8 control-label">
                                <img src="{{ asset('/img/cclogos.gif') }}" width="199" height="30" alt="Visa, Master Card, American Express, Discover">
                            </label>
                        </div>

                         <div class="form-group ">
                            <label class="col-md-4 control-label ">Price </label>
                            
                            <label class="col-md-6 control-label  col-md-pull-3">
                                &pound;{{$amount}} every 30 days
                            </label>
                        </div>


                        <div class="form-group number ">
                            <label class="col-md-4 control-label">Card Number</label>
                            
                            <div class="col-md-6">
                                <input type="text" data-stripe="number" 
                                        class="form-control input-medium bfh-phone " 
                                        data-format="dddd dddd dddd dddd">
                                
                                <span class="help-block number hidden">
                                    <strong></strong>
                                </span>
                                

                            </div>
                        </div>

                        <div class="form-group cvc"> 
                            <label class="col-md-4 control-label">CVC</label>
                            
                            <div class="col-md-6">
                                <input type="text" data-stripe="cvc" 
                                        class="form-control input-medium bfh-phone " 
                                        data-format="ddd">
                                
                                <span class="help-block cvc hidden">
                                    <strong></strong>
                                </span>
                                

                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="col-md-4 control-label">Expiration Date</label>
                            
                            <div class="col-md-3 exp_month">
                                <select class="form-control" data-stripe="exp-month" >
                                    @foreach ($month as $code => $value)
                                        <option value="{{$code}}" > {{$value}}</option>
                                    @endforeach
                                </select>

                                <span class="help-block exp_month hidden">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="col-md-3 exp_year">
                                <select class="form-control" data-stripe="exp-year" >
                                    @for ($d = date('Y'); $d < date('Y') + 20; $d++)
                                        <option value="{{$d}}">{{$d}}</option>
                                    @endfor
                                </select>
                                <span class="help-block exp_year hidden">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>

                        <p class="mm-hr">Billing Address</p>
                     
                         <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}">

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">City</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="city" value="{{ old('city') }}">

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">State</label>

                            <div class="col-md-6">
                                
                                <select class="form-control" name="state">
                                    @foreach ($state as $code => $value)
                                        <option value="{{$code}}" {{(old('state') == $code)? 'selected' : ''}}> {{$value}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Zip</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="zip" value="{{ old('zip') }}">

                                @if ($errors->has('zip'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zip') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Country</label>

                            <div class="col-md-6">
                                <select class="form-control" name="country">
                                    @foreach ($country as $code => $value)
                                        <option value="{{$code}}" {{(old('country') == $code)? 'selected' : ''}}> {{$value}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       
                    </div>
                </div>

                <button type="submit" class="btn btn-primary hidden"></button>
            </form>

            <div class="panel panel-default">
                <div class="panel-heading">Coupons</div>
                <div class="panel-body">
                    <div id="coupon-group" class="form-group">
                        <div class="col-md-4  col-md-offset-4">
                            <input type="text" id="coupon-subscription" class="form-control" name="coupon" value="{{ old('coupon') }}">
                            <span class="help-block hidden">
                                <strong>Invalid Coupon</strong>
                            </span>
                        </div>

                        <div class="col-md-4">
                            <button type="submit" onclick="onCheckCouponValidity()" class="btn btn-default">
                               Apply Coupon
                            </button>
                        </div>
                    </div>

                </div>
            </div>

        </div>


        <div class="col-md-4" id="checkout_amplifyDIV_1">
            <div id="checkout_amplifyDIV_2">
                <h2 id="checkout_amplifyH2_3">
                    <span id="checkout_amplifySPAN_4">Option2 &pound;{{$amount}} + VAT</span>
                </h2>
                <p id="checkout_amplifyP_5">
                    <span id="checkout_amplifySPAN_6"></span>
                </p>
                <ul id="checkout_amplifyUL_7">
                    <li id="checkout_amplifyLI_8">
                        Post your job vacancies to our job portal and reach the candidates every company desires.
                    </li>
                    <li id="checkout_amplifyLI_9">
                        Featured job posts on our job portal
                    </li>
                    <li id="checkout_amplifyLI_10">
                        Careers portal complete with your branding
                    </li>
                    <li id="checkout_amplifyLI_11">
                        Up to <strong id="checkout_amplifySTRONG_12">10</strong> roles amplified weekly to our Social media Channels
                    </li>
                    <li id="checkout_amplifyLI_13">
                        Up to <strong id="checkout_amplifySTRONG_14">10</strong> roles amplified weekly to the Job Boards (<strong id="checkout_amplifySTRONG_15">Monster</strong>, <strong id="checkout_amplifySTRONG_16">Indeed</strong>, <strong id="checkout_amplifySTRONG_17">Adzuna</strong>, <strong id="checkout_amplifySTRONG_18">Dice</strong>, <strong id="checkout_amplifySTRONG_19">Technojobs</strong>, <strong id="checkout_amplifySTRONG_20">CW Jobs</strong>)
                    </li>
                    <li id="checkout_amplifyLI_21">
                        <strong id="checkout_amplifySTRONG_22">2</strong> paid Facebook Campaigns per month
                    </li>
                    <li style="font: normal normal normal normal 13.44px / 16.8px 'Open Sans';">Bespoke Twitter account + following build and content for your company managed by Job Holler</li>
                    <li style="font: normal normal normal normal 13.44px / 16.8px 'Open Sans';">Enhanced employer branding through published blogs and articles</li>
                    <li id="checkout_amplifyLI_23">
                        Monthly report
                    </li>
                </ul>
                <p id="checkout_amplifyP_24">
                </p>
                <ul id="checkout_amplifyUL_25">
                    <li id="checkout_amplifyLI_26">
                        <span id="checkout_amplifySPAN_27">Price:</span> <span id="checkout_amplifySPAN_28">
                        &pound;{{$total}} every 30 days</span>
                    </li>
                    <div id="checkout_amplifyDIV_29">
                    </div>
                    <li id="checkout_amplifyLI_30">
                        <span id="checkout_amplifySPAN_31">Discount:</span> <span id="checkout_amplifySPAN_32"><span id="checkout_amplifySPAN_33">£0.00</span></span>
                    </li>
                </ul>
            </div> <span id="checkout_amplifySPAN_35">Total Price:</span> <span id="checkout_amplifySPAN_36"><span id="checkout_amplifySPAN_37">&pound;{{$total}}</span></span>

            <div id="checkout_amplifyDIV_38">
                <div id="checkout_amplifyDIV_39">
                    <p id="checkout_amplifyP_40">
                        <a href="javascript: mmjs.checkout(true);" onclick="$('#submit-form-registration').find('button[type=submit]')[0].click();" id="checkout_amplifyA_41">Submit Order</a>
                    </p>
                </div>
                <p id="checkout_amplifyP_42">
                    Please note, we do not hold any credit card data on our servers, all payment information is encrypted Via SSL and we use Stripe to process payments.
                </p>
            </div>

        </div>
        

    </div>
</div>



@endsection



@section('scripts-files')
    <script type="text/javascript" src="{{ asset('/js/libs/stripe/stripe.min.js') }}"></script>
    <script type="text/javascript">
        (function () {

            onCheckCouponValidity = function () {
                $.ajax({
                  type: 'PUT',
                  url: "{{ action('GuestUserController@postAjaxCheckCouponIgnite') }}",
                  data: {
                    '_token' : $('input[name=_token]').val(),
                    'coupon' : document.getElementById('coupon-subscription').value
                  },
                  dataType: 'json',
                  success: function(data){
                        console.log(data);
                        var coupon = $('#coupon-group');
                        if (coupon.hasClass('has-error')) {
                            coupon.removeClass('has-error');
                            var msg = coupon.find('.help-block');
                            if (!msg.hasClass('hidden')) {
                                msg.addClass('hidden');
                            }
                        }
                        $('#checkout_amplifySPAN_33').html("&pound;" + data.amount_off);
                        $('#checkout_amplifySPAN_37').html("&pound;" + data.discounted);

                        if (document.getElementById('coupon-input-field') === null) {
                            $('<input>', {
                                id: 'coupon-input-field',
                                type: 'hidden',
                                name: 'coupon',
                                value: document.getElementById('coupon-subscription').value
                            }).appendTo($('#submit-form-registration'));
                        } else {
                            $('#coupon-input-field').val(document.getElementById('coupon-subscription').value);
                        }

                  },
                  error: function(data){
                        var coupon = $('#coupon-group');
                        if (!coupon.hasClass('has-error')) {
                            coupon.addClass('has-error');
                            var msg = coupon.find('.help-block');
                            if (msg.hasClass('hidden')) {
                                msg.removeClass('hidden');
                            }
                        }
                        $('#coupon-input-field').remove();
                        
                        $('#checkout_amplifySPAN_33').html("&pound; 0.00");
                        $('#checkout_amplifySPAN_37').html("&pound;{{$total}}");
                  }
                });
            };
            var StringBilling = {
                publishable_key: '{{Config::get("services.stripe.key")}}',
                init: function () {
                    this.form = $('#submit-form-registration');
                    this.submitButton = $('#checkout_amplifyA_41'); 
                    Stripe.setPublishableKey(this.publishable_key);
                    this.bindEvents();
                },
                bindEvents: function () {
                    this.form.on('submit', $.proxy(this.sendToken, this));
                },
                sendToken: function (event) {
                    if (this.error_message !== undefined) {
                        if (!this.error_message.hasClass('hidden')) {
                            this.error_message.addClass('hidden');
                        }
                    }
                    if (this.input_wrapper !== undefined) {
                        if (this.input_wrapper.hasClass('has-error')) {
                            this.input_wrapper.removeClass('has-error');
                        }   
                    }

                    this.submitButton.prop('disabled', true);
                    Stripe.createToken(this.form, $.proxy(this.stripeResponseHandler, this));
                    event.preventDefault();
                },
                stripeResponseHandler: function (status, response) {
                    this.submitButton.prop('disabled', false);
                    if (status === 402) {
                        var error_name = response.error.param;
                        this.input_wrapper = this.form.find('div.' + error_name);
                        this.input_wrapper.addClass('has-error');
                        this.error_message = this.input_wrapper.find('span.' + error_name);
                        this.error_message.removeClass('hidden');
                        this.error_message.find('strong').text(response.error.message);
                    } else if (status === 200) {
                        $('<input>', {
                            type: 'hidden',
                            name: 'stripeToken',
                            value: response.id
                        }).appendTo(this.form);
                        this.form[0].submit();
                    }
                    console.log(status, response);
                }
            };

            StringBilling.init();
        })();
    </script>
@endsection