@extends('layouts.auth')

@section('content')
<div class="page-signin" >
    <div class="signin-header">
        <section class="logo text-center">
            <a href="/">
                <h1 style="background-image: url('img/logo.png');
                           background-size: 100% 100%;
                            width: 360px;
                            height: 100px;
                            margin: 0 auto;
                            margin-top: 5px;"></h1>
            </a>
        </section>
    </div>

    <div class="signin-body">
        <div class="container">
            <div class="form-container">

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                    {!! csrf_field() !!}
                    <fieldset>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-group input-group-first">
                                <span class="input-group-addon">
                                    <span class="ti-email"></span>
                                </span>
                                <input type="email"
                                       class="form-control input-lg"
                                       placeholder="E-Mail Address"
                                       value="{{ old('email') }}"
                                       name="email"
                                       >
                                 @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="ti-lock"></span>
                                </span>
                                <input type="password"
                                       class="form-control input-lg"
                                       placeholder="Password"
                                       name="password"
                                       >
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label class="ui-checkbox"><input name="checkbox1" name="remember" type="checkbox" >
                                    <span class="checkbox-style" style="color: #CCCCCC;">Remember Me</span></label>
                                </div>
                            </div>
                        </div>
                        <style type="text/css">
                        .checkbox-style {color: #CCCCCC;}
                        .ui-checkbox input[type="checkbox"]:checked+span.checkbox-style:after
                            {left: 5px;background-color: #858B92;}
                        .ui-checkbox input[type=checkbox]:checked+span.checkbox-style:before,
                        .ui-checkbox input[type=checkbox]+span.checkbox-style:hover:before 
                            {border: 1px solid #858B92;}
                        </style>

                        <div class="divider divider-xl"></div>
                        <div class="form-group">
                            <button 
                               type="submit"
                               class="btn btn-success btn-lg btn-block text-center">Log in</button>
                        </div>

                    </fieldset>
                </form>

                <div class="divider"></div>
                <section class="additional-info">
                    <p class="text-right">
                        <a href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                        <!-- <span> | </span> -->
                        <!-- <a href="#/page/signup">Sign up</a> -->
                    </p>
                </section>
                
            </div>
        </div>
    </div>
</div>
@endsection


