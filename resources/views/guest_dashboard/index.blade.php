@extends('layouts.guest')

@section('content')

<div id="content" class="content-container" ng-controller="RecruitmentCtrl">
  {!! html_entity_decode($angular_directive_alert) !!}
  <section data-ng-view class="view-container [[main.pageTransition.class]]"></section>
</div>
@endsection


@section('scripts-files')
<!-- <script src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places" type="text/javascript"></script> -->
<script type="text/javascript">
	(function () {
		
		$route_list = JSON.parse('{!!$route_list!!}');
		$_token = '{{$_token}}';
		
	})();
</script>
@endsection